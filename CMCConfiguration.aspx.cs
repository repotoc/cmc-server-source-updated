﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CMCConfiguration : System.Web.UI.Page
{
    object lockTarget = new object();
    string ip = (string)System.Configuration.ConfigurationSettings.AppSettings["ip"];
    int port = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["port"]);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["username"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                lock (lockTarget)
                {
                    AppDB.Logger.Info("Initiating Create CMC configuration.");
                    //AppDB.Logger.Info("Checking for Application Name exists in DB... App Name: " + ddlappname.SelectedItem.Text);
                    AppDB.Logger.Info("Checking for Atleast One Application Name exists in DB..");
                    if (AppDB.securityDB.RegApps.ToList().Count > 0)
                    {
                        lock (lockTarget)
                        {
                            AppDB.Logger.Info("Fetching all the applications and bind to the dropdownlist");
                            ddlApp.DataSource = AppDB.securityDB.RegApps.ToList();
                            ddlApp.DataBind();
                        }
                    }
                }
            }
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        AppDB.Logger.Info("Initiating Post Application Configuration: " + txtConfigName.Text + " Started.");
        if (!string.IsNullOrEmpty(txtConfigName.Text) && !string.IsNullOrEmpty(txtclientloc.Text) && !string.IsNullOrEmpty(txtftppwd.Text) && !string.IsNullOrEmpty(txtFtpPath.Text) && !string.IsNullOrEmpty(txtFtpusername.Text)  && ddlApp.SelectedItem.Text!="Select")
        {
            AppDB.Logger.Info("Validating Application configuration exists in DB or not.");
            if (!AppDB.securityDB.CMCConfig.ToList().Exists(c => c.ConfigurationName.Equals(txtConfigName.Text, StringComparison.OrdinalIgnoreCase)))
            {
                AppDB.Logger.Info("Validating Application Configuration exists");

                if (AppDB.securityDB.RegApps.ToList().Exists(c => c.AppName.Equals(ddlApp.SelectedItem.Text)))
                {
                    try
                    {
                        lock (lockTarget)
                        {
                            LOTServerDBSecurity.Models.CMCConfiguration cmcconfig = new LOTServerDBSecurity.Models.CMCConfiguration();
                            cmcconfig.ClientPath = txtclientloc.Text.Trim();
                            cmcconfig.ConfigurationName = txtConfigName.Text.Trim();
                            string strSelectApp = ddlApp.SelectedItem.Text;
                            cmcconfig.FtpPath = txtFtpPath.Text.Trim();
                            cmcconfig.FtpUserName = txtFtpusername.Text.Trim();
                            cmcconfig.FtpPassword = txtftppwd.Text.Trim();
                            cmcconfig.LaunchAppPath = txtapppath.Text.Trim();
                            cmcconfig.IsActive = true;
                            lock (lockTarget)
                            {
                                cmcconfig.RegApp = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(strSelectApp)).FirstOrDefault();
                            }
                            cmcconfig.ServerDownloadPath = cmcconfig.FtpPath;
                            cmcconfig.UpdatedTime = DateTime.Now;
                            AppDB.securityDB.CMCConfig.Add(cmcconfig);
                            AppDB.securityDB.SaveChanges();
                            myfashionsServerSDK.RefreshAPI.RefreshWEBAPI refreshapi = new myfashionsServerSDK.RefreshAPI.RefreshWEBAPI(ip, port);
                            refreshapi.RefreshDB();
                        }
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Configuration Successfully Saved'); window.location='" + Request.ApplicationPath + "ViewCMC.aspx';", true);
                        LogHelper.Logger.Info("Ended Submit Button in CMC Configuration");
                    }
                    catch (Exception ex)
                    {
                        AppDB.Logger.ErrorException(ex.Message, ex);
                    }
                }
                else
                {
                    AppDB.Logger.Info("Invalid Application Configuration.");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Invalid Application Configuration.');", true);
                }
            }
            else
            {
                AppDB.Logger.Info("Application Configuration already registered.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('CMC Name already existed.');", true);
            }
        }
        else
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Required fields should not be empty.');", true);
        
    }
    private void clearAll()
    {
        txtclientloc.Text = "";
        txtConfigName.Text = "";
        txtFtpPath.Text = "";
        txtftppwd.Text = "";
        txtFtpusername.Text = "";
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("ViewCMC.aspx");
    }
}