﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KeyGenerator;
using LOTServerDBSecurity.Models;

public partial class KeyGeneratorPage : System.Web.UI.Page
{
    KeyGen generateKey;
    object lockTarget = new object();
    string ip = (string)System.Configuration.ConfigurationSettings.AppSettings["ip"];
    int port = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["port"]);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["username"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                lock (lockTarget)
                {
                    AppDB.Logger.Info("Initiating Create Key Generator Page.");
                    //AppDB.Logger.Info("Checking for Application Name exists in DB... App Name: " + ddlappname.SelectedItem.Text);
                    AppDB.Logger.Info("Checking for Atleast One Application Name exists in DB..");
                    if (AppDB.securityDB.RegApps.ToList().Count > 0)
                    {
                        lock (lockTarget)
                        {
                            AppDB.securityDB = new LOTServerDBSecurity.DBFolder.LotServerDBSecurityContext();
                            AppDB.Logger.Info("Fetching all the applications and bind to the dropdownlist");
                            ddlApp.DataSource = AppDB.securityDB.RegApps.ToList();
                            ddlApp.DataBind();
                        }
                    
                    }
                }
            }
        }
    }
    protected void btnGenerate_Click(object sender, EventArgs e)
    {
        try
        {
            bool isPressed = false;
            if (!isPressed)
            {
                isPressed = true;
                GenerateKey();
            }
        }
        catch (Exception ex)
        {
            LogHelper.Logger.ErrorException("Exception: btnGenerate_Click  : KeyGeneratorPage: " + ex.Message, ex);
        }
    }

    private void GenerateKey()
    {
        LogHelper.Logger.Info("Validate before generate key.");
        RegisteredApplications selectedApp = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(ddlApp.SelectedItem.Text, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

        StoreAdmin selectedAdmin = AppDB.securityDB.StoreAdmin.Where(c => c.UserName.Equals(ddlStoreAdmins.SelectedItem.Text, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
        if (selectedApp != null && selectedAdmin != null)
        {
            if (selectedApp.ConfidentialKey != null)
            {
                if (txtconfidential.Text.Trim().Equals(selectedApp.ConfidentialKey.SecretKey))
                {
                        LogHelper.Logger.Info("Started Generating the Key");
                        generateKey = new KeyGen(AppDB.securityDB, selectedApp, selectedAdmin)
                        {
                            SystemName = txtSystemName.Text.Trim(),
                            AppGUID = txtguid.Text.Trim(),
                            CustomerEmail = txtCustomerEmail.Text.Trim(),
                            ContactNumber=txtMobileNumber.Text.Trim(),
                            CustomerName = txtStoreName.Text.Trim(),
                            HDDDeviceID = txtdeviceId.Text.Trim(),
                            HDDInterType = txthddintertype.Text.Trim(),
                            HDDMediaType = txtmediatype.Text.Trim(),
                            HDDSerial = txthddserial.Text.Trim(),
                            IsEnabled = chkEnabled.Checked,
                            IsUpdateAvailable = chkavail.Checked,
                            SelectedApplication = ddlApp.SelectedItem.Text
                        };
                        LogHelper.Logger.Info("Ended Generating the Key");
                        string generatedKey = generateKey.GenerateKey();
                        txtGeneratedKey.Text = generatedKey;
                        GetDisabled();
                    #region Commented
                    //LOTServerDBSecurity.Models.SecurityKey security = new LOTServerDBSecurity.Models.SecurityKey();
                    //lock (lockTarget)
                    //{
                    //    List<LOTServerDBSecurity.Models.SecurityKey> SecurityData = AppDB.securityDB.SecurityKey.Where(c => c.HDDSerial == txthddserial.Text.Trim()).ToList();
                    //    if (SecurityData.Count == 0)
                    //    {
                    //        security.SysName = txtSystemName.Text.Trim();
                    //        security.ApplicationName = ddlApp.SelectedItem.Text.Trim();
                    //        security.GUID = txtguid.Text.Trim();
                    //        security.CustEMail = txtCustomerEmail.Text;
                    //        security.HDDDeviceType = txtdeviceId.Text.Trim();
                    //        security.HDDInterfaceType = txthddintertype.Text.Trim();
                    //        security.HDDMediaType = txtmediatype.Text.Trim();
                    //        security.HDDSerial = txthddserial.Text.Trim();
                    //        security.AuthCode = txtGeneratedKey.Text.Trim();
                    //        security.Name = txtCustomerName.Text.Trim();
                    //        security.ContactNumber = txtMobileNumber.Text.Trim();
                    //        if (chkEnabled.Checked == true)
                    //        {
                    //            security.ActivatedDate = DateTime.Now;
                    //        }
                    //        else
                    //        {
                    //            security.DeActivatedDate = DateTime.Now;
                    //        }

                    //        AppDB.securityDB.SecurityKey.Add(security);
                    //        AppDB.securityDB.SaveChanges();
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Key Generated Successfully');", true);
                    //    }
                    //    else
                    //    {
                    //        //security.SysName = txtSystemName.Text.Trim();
                    //        //security.ApplicationName = ddlApp.SelectedItem.Text.Trim();
                    //        //security.GUID = txtguid.Text.Trim();
                    //        //security.CustEMail = txtCustomerEmail.Text;
                    //        //security.HDDDeviceType = txtdeviceId.Text.Trim();
                    //        //security.HDDInterfaceType = txthddintertype.Text.Trim();
                    //        //security.HDDMediaType = txtmediatype.Text.Trim();
                    //        //security.HDDSerial = txthddserial.Text.Trim();
                    //        //security.AuthCode = txtGeneratedKey.Text.Trim();
                    //        if (chkEnabled.Checked == true)
                    //        {
                    //            security.ActivatedDate = DateTime.Now;
                    //        }
                    //        else
                    //        {
                    //            security.DeActivatedDate = DateTime.Now;
                    //        }
                    //        AppDB.securityDB.SaveChanges();
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Key Updated Successfully');", true);
                    //    }
                    //}
                    #endregion
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Invalid confidential key.');", true);
                    LogHelper.Logger.Info("Invalid confidential key.");
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Confidential key doesn't exist.');", true);
                LogHelper.Logger.Info("Confidential key doesn't exist.");
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Required fields should not be empty.');", true);
            LogHelper.Logger.Info("Required fields should not be empty.");
        }
    }

    private void GetDisabled()
    {
        txtconfidential.ReadOnly = true;
        txtCustomerEmail.ReadOnly = true;
        ddlApp.Enabled = false;
        ddlStoreAdmins.Enabled = false;
        txtStoreName.ReadOnly = true;
        txtCustomerEmail.ReadOnly = true;
        txtMobileNumber.ReadOnly = true;
        txtconfidential.ReadOnly = true;
        txtSystemName.ReadOnly = true;
        txtguid.ReadOnly = true;
        txthddintertype.ReadOnly = true;
        txthddserial.ReadOnly = true;
        txtdeviceId.ReadOnly = true;
        txtmediatype.ReadOnly = true;

    }
    protected void btnSendMail_Click(object sender, EventArgs e)
    {
        try
        {
            LogHelper.Logger.Info("Started Sending generated key through mail");
                GenerateKey();
                generateKey.SaveAndSendEmail();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Mail Sent Successfully'); window.location='" + Request.ApplicationPath + "ViewKeys.aspx';", true);
            LogHelper.Logger.Info("Ended Sending generated key through mail");
            myfashionsServerSDK.RefreshAPI.RefreshWEBAPI refreshapi = new myfashionsServerSDK.RefreshAPI.RefreshWEBAPI(ip, port);
            refreshapi.RefreshDB();
        }
        catch (Exception ex)
        {
            myfashionsServerSDK.RefreshAPI.RefreshWEBAPI refreshapi = new myfashionsServerSDK.RefreshAPI.RefreshWEBAPI(ip, port);
            refreshapi.RefreshDB();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Mail Sent Successfully'); window.location='" + Request.ApplicationPath + "ViewKeys.aspx';", true);
            LogHelper.Logger.ErrorException("Exception: btnSendMail_Click  : KeyGeneratorPage: " + ex.Message, ex);
        }
    }

    private void clearAll()
    {
         txtSystemName.Text="";
         txtconfidential.Text="";
        txtguid.Text="";
        txtdeviceId.Text="";
        txthddintertype.Text="";
        txthddserial.Text="";
        txtmediatype.Text="";
        txtSystemName.Text="";
    }
    protected void ddlApp_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlApp.SelectedItem.Text != "Select")
        {
            RegisteredApplications selectedRegApp = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(ddlApp.SelectedItem.Text.Trim(), StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            if (selectedRegApp != null)
            {
                if (selectedRegApp.LstStoreAdmins != null)
                {
                    if (selectedRegApp.LstStoreAdmins.Count > 0)
                    {
                        ddlStoreAdmins.Items.Clear();
                        ddlStoreAdmins.Items.Add("Select");
                        ddlStoreAdmins.DataSource = selectedRegApp.LstStoreAdmins.ToList();
                        ddlStoreAdmins.DataBind();
                        txtMobileNumber.Text = "";
                        txtStoreName.Text = "";
                        txtCustomerEmail.Text = "";
                    }
                    else
                    {
                        ddlStoreAdmins.Items.Clear();
                        ddlStoreAdmins.Items.Add("Select");
                        txtMobileNumber.Text = "";
                        txtStoreName.Text = "";
                        txtCustomerEmail.Text = "";

                    }
                }
              
                if (selectedRegApp.ConfidentialKey!=null)
                {
                    List<LOTServerDBSecurity.Models.ConfidentialKey> lstConfKeys = AppDB.securityDB.ConfidentialKeys.ToList().Where(c => c.SNo.Equals(selectedRegApp.ConfidentialKey.SNo)).ToList();
                    if (lstConfKeys != null)
                    {
                        txtconfidential.Text = selectedRegApp.ConfidentialKey.SecretKey;
                    }
                    else
                    {
                        txtconfidential.Text = "";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Generate confidential Key first for this Application.');", true);
                    }
                }
                else
                {
                    txtconfidential.Text = "";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Generate confidential Key first for this Application.');", true);
                }
            }
        }
        else
        {
            ddlStoreAdmins.Items.Clear();
            ddlStoreAdmins.Items.Add("Select");
            txtconfidential.Text = "";
            txtMobileNumber.Text = "";
            txtStoreName.Text = "";
            txtCustomerEmail.Text = "";
        }
       
    }
    protected void ddlStoreAdmins_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlStoreAdmins.SelectedItem.Text != "Select")
        {
            LOTServerDBSecurity.Models.StoreAdmin strAdmin = AppDB.securityDB.StoreAdmin.Where(c => c.UserName.Equals(ddlStoreAdmins.SelectedItem.Text, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            if (strAdmin != null)
            {
                txtStoreName.Text = strAdmin.StoreName;
                txtCustomerEmail.Text = strAdmin.StoreEMail;
                txtMobileNumber.Text = strAdmin.StoreContactNumber;
            }
        }
        else
        {
            txtStoreName.Text = "";
            txtCustomerEmail.Text = "";
            txtMobileNumber.Text = "";
        }
    }
}