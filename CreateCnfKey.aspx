﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="CreateCnfKey.aspx.cs"  Inherits="CreateCnfKey" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <style>
         .table > tbody > tr > td {border:none;
            }
    </style>
     <script>
         function ValidateFieldLegth(sender, args) {
             var v = document.getElementById('<%=txtsecretkey.ClientID%>').value;
             if (v.length < 6 && v.length > 10) {

                
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
             }
             alert(v.length);
        }
</script>
      <table class="table"  style="width:50%;margin:62px 22px 0px 280px;">
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
           <tr>
            <td>Select App Name</td>
            <td><asp:DropDownList ID="ddlappname" runat="server"  DataTextField="AppName" DataValueField="SNo" AppendDataBoundItems="true" CssClass="dropdown">
                <asp:ListItem Value="-1" Text="Select"></asp:ListItem>
                </asp:DropDownList>
                <br />
            </td>
        </tr>
          <tr>
            <td>Secret Key</td>
            <td><asp:TextBox ID="txtsecretkey" runat="server" CssClass="textboxstyle" ></asp:TextBox>
                <br />
            </td>
        </tr>
          <tr>
            
            <td colspan="2" style="text-align:center">
                <asp:Button ID="btnSubmit" runat="server"  Text="Submit"  CssClass="btn"   OnClick="btnSubmit_Click"/>
                <asp:Button ID="btnCancel" runat="server"  Text="Cancel"  CssClass="btn" CausesValidation="false" OnClick="btnCancel_Click"/>
            </td>
        </tr>
          </table>
</asp:Content>

