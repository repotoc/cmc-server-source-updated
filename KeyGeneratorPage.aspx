﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="KeyGeneratorPage.aspx.cs" Inherits="KeyGeneratorPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <style>
         .table > tbody > tr > td {border:none;
            }
    </style>
    <table class="table"  style="width:80%;margin:50px 106px;">
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
             <td>Select Application:</td>
            <td>
                <asp:DropDownList ID="ddlApp" runat="server" AppendDataBoundItems="true" CssClass="textboxstyle" AutoPostBack="true"  DataTextField="AppName" DataValueField="SNo" OnSelectedIndexChanged="ddlApp_SelectedIndexChanged">
                    <asp:ListItem Value="-1">Select</asp:ListItem>
                </asp:DropDownList>
                <br />
            </td>
             <td>Select User:</td>
            <td>
                <asp:DropDownList ID="ddlStoreAdmins" runat="server" AppendDataBoundItems="true" CssClass="textboxstyle" AutoPostBack="true"  DataTextField="UserName" DataValueField="SNo" OnSelectedIndexChanged="ddlStoreAdmins_SelectedIndexChanged">
                    <asp:ListItem Value="-1">Select</asp:ListItem>
                </asp:DropDownList>
                <br />
            </td>
        </tr>
        <tr>
            <td  style="width:15%">Store Name:</td>
            <td style="width:35%"><asp:TextBox ID="txtStoreName" runat="server" CssClass="textboxstyle" Enabled="false"></asp:TextBox>
                <br />
            </td>
            <td>Store Email:</td>
            <td><asp:TextBox ID="txtCustomerEmail" runat="server" CssClass="textboxstyle"  Enabled="false" ></asp:TextBox>
                <br />
            </td>
        </tr>
         <tr>
             <td>Mobile Number:</td>
            <td><asp:TextBox ID="txtMobileNumber" runat="server" CssClass="textboxstyle" Enabled="false" ></asp:TextBox>
                <asp:RegularExpressionValidator ID="rev1" runat="server" ControlToValidate="txtMobileNumber" Display="Dynamic" Font-Size="Smaller" ValidationExpression="[0-9]{10}" ForeColor="Red" ErrorMessage="Accepts only Numerics"></asp:RegularExpressionValidator>
            </td>
              <td>Confidential Text:</td>
            <td><asp:TextBox ID="txtconfidential" runat="server" CssClass="textboxstyle" Enabled="false" ></asp:TextBox>
                
            </td>
        </tr>
        <tr>
            <td style="width:15%">System Name:</td>
            <td style="width:35%"><asp:TextBox ID="txtSystemName" runat="server" CssClass="textboxstyle" ></asp:TextBox></td>
            <td>Enter GUID:</td>
            <td><asp:TextBox ID="txtguid" runat="server" CssClass="textboxstyle" ></asp:TextBox></td>
        </tr>
         <tr>
            <td>HDD Serial:</td>
            <td><asp:TextBox ID="txthddserial" runat="server" CssClass="textboxstyle" ></asp:TextBox>
                <br />
            </td>
           <td>HDD Inter Type:</td>
            <td><asp:TextBox ID="txthddintertype" runat="server" CssClass="textboxstyle" ></asp:TextBox></td>
        </tr>
         <tr>
            <td>Device ID:</td>
            <td><asp:TextBox ID="txtdeviceId" runat="server" CssClass="textboxstyle" ></asp:TextBox>
                <br />
            </td>
            <td>Media Type:</td>
            <td><asp:TextBox ID="txtmediatype" runat="server" CssClass="textboxstyle" ></asp:TextBox></td>
        </tr>
         <tr>
           <td></td>
            <td><asp:CheckBox ID="chkEnabled" runat="server" Checked="true"  Visible="false" Text="Is Enabled"/>
           </td>
            <td></td>
             <td><asp:CheckBox ID="chkavail" runat="server" Checked="true" Visible="false" Text="Is Update Available" /></td>
        </tr>
        <tr>
           <td></td>
            <td>
            <asp:Button ID="btnGenerate" runat="server" Text="Generate" OnClick="btnGenerate_Click" /></td>
             <td>Generated Key:</td>
            <td><asp:TextBox ID="txtGeneratedKey" runat="server" Enabled="false" CssClass="textboxstyle" ></asp:TextBox>
                <br />
                <asp:Button ID="btnSendMail" runat="server" Text="Save & Send Mail" OnClick="btnSendMail_Click"/>
            </td>
        </tr>
    </table>
</asp:Content>

