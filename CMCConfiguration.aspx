﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="CMCConfiguration.aspx.cs" Inherits="CMCConfiguration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <style>
         .table > tbody > tr > td {border:none;
            }
    </style>
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
   <%--  <script type = "text/javascript">
      function ValidateRadio(sender, args) {
          args.IsValid = $("[name=V1]:checked").length > 0;
      }
    </script>--%>
    <table class="table"  style="width:80%;margin:62px 22px 0px 152px;">
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Select Application:</td>
            <td>
                <asp:DropDownList ID="ddlApp" runat="server"   AppendDataBoundItems="true" DataTextField="AppName" DataValueField="SNo" CssClass="textboxstyle">
                    <asp:ListItem Value="-1">Select</asp:ListItem>
                </asp:DropDownList>

                <br />

            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Configuration Name:</td>
            <td><asp:TextBox ID="txtConfigName" runat="server" CssClass="textboxstyle" ></asp:TextBox>
                <asp:RegularExpressionValidator ID="rev1" runat="server" ControlToValidate="txtConfigName" ErrorMessage="Name Should Accept Alphabets Only" ForeColor="Red" Font-Size="Smaller" ValidationExpression="[a-zA-Z][a-zA-Z ]+"  Display="Dynamic" ></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "txtConfigName" ID="RegularExpressionValidator3" ForeColor="Red" Font-Size="Smaller" ValidationExpression = "^[\s\S]{3,}$" runat="server" ErrorMessage="Minimum 3  characters required."></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "txtConfigName" ID="RegularExpressionValidator4" ForeColor="Red" Font-Size="Smaller" ValidationExpression ="^[\s\S]{0,30}$" runat="server" ErrorMessage="Limit Should Not Exceed 30 characters."></asp:RegularExpressionValidator>
                <br />
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>FTP Path:</td>
            <td><asp:TextBox ID="txtFtpPath" runat="server" required="required"   Text="ftp://" CssClass="textboxstyle" ></asp:TextBox>
                <br />
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
          <tr>
            <td>FTP UserName:</td>
 
            <td><asp:TextBox ID="txtFtpusername" runat="server" required="required" CssClass="textboxstyle" ></asp:TextBox>

                <br />
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
         <tr>
            <td>FTP Password:</td>
            <td><asp:TextBox ID="txtftppwd" runat="server" TextMode="Password" required="required" CssClass="textboxstyle" ></asp:TextBox>
                <br />
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
         <tr>
            <td>Client Location Path:</td>
            <td><asp:TextBox ID="txtclientloc" runat="server" CssClass="textboxstyle" required="required" ></asp:TextBox>
                <br />
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Launch App Path:</td>
            <td><asp:TextBox ID="txtapppath" runat="server" CssClass="textboxstyle" required="required" ></asp:TextBox>
             <br />
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
             <td colspan="2">
                <asp:Label ID="lblenabled" runat="server" Text="CMC Status" Visible="false"></asp:Label>

              <%--   <asp:RadioButton ID="radEnable" runat="server" Text="Enable" GroupName="V1" ToolTip="It will be Enabled for all the Users and Keys" />
                 <asp:RadioButton ID="radDisable" runat="server" Text="Disable" GroupName="V1" ToolTip="It will be Disabled for all the Users and Keys" />--%>
                <asp:RadioButtonList ID="radList" runat="server" Width="300px" Visible="false"   RepeatColumns="2"  RepeatDirection="Horizontal" Height="20px" >
                    <asp:ListItem>Enable</asp:ListItem>
                    <asp:ListItem>Disable</asp:ListItem>
                </asp:RadioButtonList>
                <%-- <asp:CustomValidator ID="cvRadioButtonGroup" runat="server" ErrorMessage="*make a selection"/>--%>
             </td>
        </tr>

          <tr>
            <td>&nbsp;</td>
            <td>
                <br />
                <%--OnClientClick="valButton()"--%>
                <asp:Button ID="btnSubmit" runat="server"  Text="Submit"   CssClass="btn"    OnClick="btnSubmit_Click" />
                <asp:Button ID="btnCancel" runat="server"  Text="Cancel"   CssClass="btn" CausesValidation="false"  OnClick="btnCancel_Click"/>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        </table>
</asp:Content>

