﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtusername.Text != null && txtpwd.Text != null)
            {
                if (txtusername.Text == "admin" && txtpwd.Text == "admin")
                {
                    Session["username"] = txtusername.Text;
                    Response.Redirect("Home.aspx");
                    
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Enter Valid UserName And Password');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Enter Required Fields');", true);
            }
            
        }
        catch (Exception ex)
        {
            
        }
      
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtusername.Text = "";
        txtpwd.Text = "";

    }
}