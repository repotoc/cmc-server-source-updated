﻿using LOTServerDBSecurity.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ViewUsers : System.Web.UI.Page
{
    object lockTarget = new object();
    DataTable dt, dtSearch;
    DataRow dr, dr1;
    string ip = (string)System.Configuration.ConfigurationSettings.AppSettings["ip"];
    int port = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["port"]);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["username"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                lock (lockTarget)
                {
                    
                    AppDB.Logger.Info("Initiating View Users Or Store Admins.");
                    AppDB.Logger.Info("Checking for Atleast One User or StoreAdmin exists in DB..");
                    if (AppDB.securityDB.StoreAdmin.ToList().Count > 0)
                    {
                        GetTotalData();
                        ddlappname.DataSource = AppDB.securityDB.RegApps.ToList();
                        ddlappname.DataBind();
                    }
                }
            }
        }
    }
    protected void btnnewuser_Click(object sender, EventArgs e)
    {
        Response.Redirect("UserRegistration.aspx");
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        AppDB.Logger.Info("Initiating Update Store Admin. Admin ID " + lblid.Text + " Admin Name: " + txtusername.Text);
        if (Convert.ToInt64(lblid.Text) > 0 && !string.IsNullOrEmpty(txtusername.Text) && !string.IsNullOrEmpty(txtpwd.Text) && !string.IsNullOrEmpty(txtuserid.Text))
        {
            lock (lockTarget)
            {
                long id = Convert.ToInt64(lblid.Text);
                AppDB.Logger.Info("Checking for User exist in DB.");
                if (AppDB.securityDB.StoreAdmin.ToList().Exists(c => c.SNo.Equals(id)))
                {
                    AppDB.Logger.Info("Checking for Registered Application exist in DB.");
                    if (AppDB.securityDB.RegApps.ToList().Exists(c => c.AppName.Equals(txtApplicationName.Text)) && AppDB.securityDB.CMCConfig.ToList().Exists(c => c.ConfigurationName.Equals(txtConfigName.Text)))
                    {
                        lock (lockTarget)
                        {
                            LOTServerDBSecurity.Models.StoreAdmin userPerm = AppDB.securityDB.StoreAdmin.Where(c => c.SNo.Equals(id)).FirstOrDefault();
                            userPerm.Password = txtpwd.Text.Trim();
                            userPerm.UpdatedTime = DateTime.Now;
                            userPerm.UserName = txtusername.Text.Trim();
                            userPerm.StoreName = txtStoreName.Text.Trim();
                            userPerm.StoreEMail = txtStoreEmail.Text.Trim();
                            userPerm.StoreContactNumber = txtStoreContactNumber.Text.Trim();
                            AppDB.securityDB.SaveChanges();
                            myfashionsServerSDK.RefreshAPI.RefreshWEBAPI refreshapi = new myfashionsServerSDK.RefreshAPI.RefreshWEBAPI(ip, port);
                            refreshapi.RefreshDB();
                            clearAll();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('User Updated Successfully'); window.location='" + Request.ApplicationPath + "ViewUsers.aspx';", true);
                            EditPanel.Visible = false;
                        }
                       
                    }
                    else
                    {
                        AppDB.Logger.Info("Invalid Registered Application.");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Invalid Registered Application.');", true);
                    }
                }
                else
                {
                    AppDB.Logger.Info("Store Admin not found.");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Store Admin not found.');", true);
                }
            }
        }
        else
        {
            AppDB.Logger.Info("Invalid Store Admin.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Invalid Store Admin.');", true);

        }

    }
    private void clearAll()
    {
        txtuserid.Text = "";
        txtusername.Text = "";
        txtpwd.Text = "";
    }
    protected void gdviewusers_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void ddlApp_SelectedIndexChanged(object sender, EventArgs e)
    {
       
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        lock (lockTarget)
        {
            long id = Convert.ToInt64(lblid.Text);
            LOTServerDBSecurity.Models.StoreAdmin Admin = AppDB.securityDB.StoreAdmin.Where(c => c.SNo.Equals(id)).FirstOrDefault();
            txtuserid.Text = Admin.UserID;
            txtusername.Text = Admin.UserName;
            txtpwd.Text = Admin.Password;
            txtStoreName.Text = Admin.StoreName;
            txtStoreEmail.Text = Admin.StoreEMail;
            txtStoreContactNumber.Text = Admin.StoreContactNumber;
        }
    }
    protected void btnEdit_Click(object sender, EventArgs e)
    {

        Label1.Visible = false;
        bool isChecked = false;
        foreach (GridViewRow row in gdviewusers.Rows)
        {
            RadioButton rb = (RadioButton)row.FindControl("RadioButton1");
            if (rb.Checked)
            {
                isChecked = true;
                EditPanel.Visible = true;
            }
        }

        if (!isChecked)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Select AnyOne.');", true);
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        Label1.Visible = false;
        int Index;
        DataTable dt = (DataTable)ViewState["CurrentTable"];

        for (int i = 0; i < gdviewusers.Rows.Count; i++)
        {
            GridViewRow row = gdviewusers.Rows[i];
            RadioButton rb = (RadioButton)row.FindControl("RadioButton1");
            if (rb.Checked)
            {
                Index = row.RowIndex;
                lblid.Text = dt.Rows[Index]["strSno"].ToString();
                long id = Convert.ToInt64(lblid.Text);
                AppDB.Logger.Info("Initiating Deleting Store Admin ID: " + id);
                if (AppDB.securityDB.StoreAdmin.ToList().Exists(c => c.SNo.Equals(id)))
                {
                    try
                    {
                        lock (lockTarget)
                        {
                            LOTServerDBSecurity.Models.StoreAdmin userPerm = AppDB.securityDB.StoreAdmin.Where(c => c.SNo.Equals(id)).FirstOrDefault();
                            if (userPerm.LstRegKeys == null)
                            {
                                AppDB.securityDB.StoreAdmin.Remove(userPerm);
                                AppDB.securityDB.SaveChanges();
                                myfashionsServerSDK.RefreshAPI.RefreshWEBAPI refreshapi = new myfashionsServerSDK.RefreshAPI.RefreshWEBAPI(ip, port);
                                refreshapi.RefreshDB();
                                clearAll();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('User  Deleted Successfully'); window.location='" + Request.ApplicationPath + "ViewUsers.aspx';", true);
                                EditPanel.Visible = false;
                            }
                            else if (userPerm.LstRegKeys.Count == 0)
                            {
                                AppDB.securityDB.StoreAdmin.Remove(userPerm);
                                AppDB.securityDB.SaveChanges();
                                myfashionsServerSDK.RefreshAPI.RefreshWEBAPI refreshapi = new myfashionsServerSDK.RefreshAPI.RefreshWEBAPI(ip, port);
                                refreshapi.RefreshDB();
                                clearAll();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('User  Deleted Successfully'); window.location='" + Request.ApplicationPath + "ViewUsers.aspx';", true);
                                EditPanel.Visible = false;
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('First Delete the Security Keys under this User');", true);
                            }
                        }
                       
                    }
                    catch (Exception ex)
                    {
                        AppDB.Logger.ErrorException(ex.Message, ex);
                    }
                }
                else
                {
                    AppDB.Logger.Info("Invalid ID.");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Invalid ID.');", true);
                }
            }
        }
    }
    protected void imgClose_Click(object sender, ImageClickEventArgs e)
    {
        EditPanel.Visible = false;
    }
    protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
    {
        int Index;
        DataTable dt = (DataTable)ViewState["CurrentTable"];

        for (int i = 0; i < gdviewusers.Rows.Count; i++)
        {
            GridViewRow row = gdviewusers.Rows[i];
            RadioButton rdb = (RadioButton)row.FindControl("RadioButton1");
            if (rdb.Checked == true)
            {
                Index = row.RowIndex;
                lblid.Text = dt.Rows[Index]["strSno"].ToString();
                long id = Convert.ToInt64(lblid.Text);
                lock (lockTarget)
                {
                    lock (lockTarget)
                    {
                        LOTServerDBSecurity.Models.StoreAdmin Admin = AppDB.securityDB.StoreAdmin.Where(c => c.SNo.Equals(id)).FirstOrDefault();
                        if (Admin != null)
                        {
                            txtuserid.Text = Admin.UserID;
                            txtusername.Text = Admin.UserName;
                            txtpwd.Text = Admin.Password;
                            txtStoreName.Text = Admin.StoreName;
                            txtStoreEmail.Text = Admin.StoreEMail;
                            txtStoreContactNumber.Text = Admin.StoreContactNumber;
                            lock (lockTarget)
                            {
                                LOTServerDBSecurity.Models.CMCConfiguration cmc = AppDB.securityDB.CMCConfig.Where(c => c.SNo.Equals(Admin.CMCConfig.SNo)).FirstOrDefault();
                                txtConfigName.Text = cmc.ConfigurationName;
                            }
                            lock (lockTarget)
                            {
                                LOTServerDBSecurity.Models.RegisteredApplications regApp = AppDB.securityDB.RegApps.Where(c => c.SNo.Equals(Admin.RegApp.SNo)).FirstOrDefault();
                                txtApplicationName.Text = regApp.AppName;
                            }
                        }
                    }
                }
            }
        }
    }
    protected void gdviewusers_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dataTable = ViewState["CurrentTable"] as DataTable;
        if (dataTable != null)
        {
            DataView dataView = new DataView(dataTable);
            dataView.Sort = e.SortExpression;
            gdviewusers.DataSource = dataView;
            gdviewusers.DataBind();
        }
    }
    protected void ddlappname_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlappname.SelectedItem.Text != "Select")
        {
            gdviewusers.DataSource = null;
            GetAppData();
        }
        else
        {
            ddlcmc.Items.Clear();
            ddlcmc.Items.Add("Select");
            GetTotalData();
        }
    }

    private void GetTotalData()
    {
        lock (lockTarget)
        {
            dt = new DataTable();
            dr = null;

            dt.Columns.Add(new DataColumn("strSno", typeof(long)));
            dt.Columns.Add(new DataColumn("strAppName", typeof(string)));
            dt.Columns.Add(new DataColumn("strConfig", typeof(string)));
            dt.Columns.Add(new DataColumn("strStoreName", typeof(string)));
            dt.Columns.Add(new DataColumn("strStoreEmail", typeof(string)));
            dt.Columns.Add(new DataColumn("strStoreContactNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("strUserName", typeof(string)));
            dt.Columns.Add(new DataColumn("strUpdatedTime", typeof(string)));
            dt.Columns.Add(new DataColumn("strIsUpdate", typeof(string)));
            dt.Columns.Add(new DataColumn("strIsActive", typeof(string)));
            foreach (var item in AppDB.securityDB.StoreAdmin.ToList())
            {
                lock (lockTarget)
                {
                    long strSno = item.SNo;
                    LOTServerDBSecurity.Models.StoreAdmin getUsers = AppDB.securityDB.StoreAdmin.Where(c => c.SNo.Equals(strSno)).FirstOrDefault();
                    if (getUsers != null)
                    {
                        String strUserId = getUsers.UserID;
                        string strUserName = getUsers.UserName;
                        string strUpdatedTime = getUsers.UpdatedTime.ToString();
                        string strStoreContactNumber = getUsers.StoreContactNumber;
                        string strStoreEmail = getUsers.StoreEMail;
                        string strStoreName = getUsers.StoreName;
                        string strIsUpdate, strIsActive;
                        if (getUsers.IsUpdateAvailable)
                        {
                            strIsUpdate = "Enable";
                        }
                        else
                        {
                            strIsUpdate = "Disable";
                        }
                        if (getUsers.IsActive)
                        {
                            strIsActive = "Active";
                        }
                        else
                        {
                            strIsActive = "InActive";
                        }

                        lock (lockTarget)
                        {
                            LOTServerDBSecurity.Models.RegisteredApplications regAppName = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(getUsers.RegApp.AppName)).FirstOrDefault();
                            if (regAppName != null)
                            {
                                if (regAppName.LstCMCConfig.ToList().Exists(c => c.SNo.Equals(getUsers.CMCConfig.SNo)))
                                {

                                    LOTServerDBSecurity.Models.CMCConfiguration cmcconfig = AppDB.securityDB.CMCConfig.Where(c => c.SNo.Equals(getUsers.CMCConfig.SNo)).FirstOrDefault();
                                    if (cmcconfig != null)
                                    {
                                        string strAppName = regAppName.AppName;
                                        dr = dt.NewRow();
                                        dr["strSno"] = strSno;
                                        dr["strAppName"] = regAppName.AppName;
                                        dr["strConfig"] = cmcconfig.ConfigurationName;
                                        dr["strStoreName"] = strStoreName;
                                        dr["strStoreEmail"] = strStoreEmail;
                                        dr["strStoreContactNumber"] = strStoreContactNumber;
                                        dr["strUserName"] = strUserName;
                                        dr["strUpdatedTime"] = strUpdatedTime;
                                        dr["strIsUpdate"] = strIsUpdate;
                                        dr["strIsActive"] = strIsActive;
                                        dt.Rows.Add(dr);
                                        //Store the DataTable in ViewState
                                        ViewState["CurrentTable"] = dt;
                                    }
                                }
                            }
                        }

                    }
                }
            }
            AppDB.Logger.Info("Fetching all the StoreAdmins from DB anf bind to the Gridview");
            gdviewusers.DataSource = dt;
            gdviewusers.DataBind();

        }
    }
    private void GetAppData()
    {
        string strStoreName, strStoreEmail, strStoreContact, strUserName, strIsActive, strIsUpdate, strIsUpdatedtime;
        long strSno;
        LOTServerDBSecurity.Models.RegisteredApplications reApp = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(ddlappname.SelectedItem.Text)).FirstOrDefault();
        if (reApp != null)
        {
            ddlcmc.DataSource = null;
            ddlcmc.Items.Clear();
            ddlcmc.Items.Add("Select");
            ddlcmc.DataTextField = "ConfigurationName";
            ddlcmc.DataValueField = "SNo";
            ddlcmc.DataSource = reApp.LstCMCConfig.ToList();
            ddlcmc.DataBind();
            lock (lockTarget)
            {
                dt = new DataTable();
                dr = null;

                dt.Columns.Add(new DataColumn("strSno", typeof(long)));
                dt.Columns.Add(new DataColumn("strAppName", typeof(string)));
                dt.Columns.Add(new DataColumn("strConfig", typeof(string)));
                dt.Columns.Add(new DataColumn("strStoreName", typeof(string)));
                dt.Columns.Add(new DataColumn("strStoreEmail", typeof(string)));
                dt.Columns.Add(new DataColumn("strStoreContactNumber", typeof(string)));
                dt.Columns.Add(new DataColumn("strUserName", typeof(string)));
                dt.Columns.Add(new DataColumn("strUpdatedTime", typeof(string)));
                dt.Columns.Add(new DataColumn("strIsUpdate", typeof(string)));
                dt.Columns.Add(new DataColumn("strIsActive", typeof(string)));
                List<LOTServerDBSecurity.Models.RegisteredApplications> lstReg = AppDB.securityDB.RegApps.ToList().Where(c => c.AppName.Equals(ddlappname.SelectedItem.Text)).ToList();
                List<LOTServerDBSecurity.Models.CMCConfiguration> lstcmc = reApp.LstCMCConfig.ToList();
                List<LOTServerDBSecurity.Models.StoreAdmin> lstStores = reApp.LstStoreAdmins.ToList();

                foreach (var item in reApp.LstCMCConfig.ToList().Distinct())
                {
                    string strConfigName = item.ConfigurationName;
                    string strFtpUserName = item.FtpUserName;
                    string strUpdatedTime = item.UpdatedTime.ToString();
                    string strAppName = reApp.AppName;
                    long Sno = item.SNo;
                    foreach (var item2 in item.RegApp.LstStoreAdmins.ToList().Where(c=>c.CMCConfig.SNo.Equals(Sno)))
                    {
                        strSno = item2.SNo;
                        strStoreName = item2.StoreName;
                        strStoreEmail = item2.StoreEMail;
                        strStoreContact = item2.StoreContactNumber;
                        strUserName = item2.UserName;

                        strIsUpdatedtime = item2.UpdatedTime.ToString();
                        if (item2.IsUpdateAvailable)
                        {
                            strIsUpdate = "Enable";
                        }
                        else
                        {
                            strIsUpdate = "Disable";
                        }
                        if (item2.IsActive)
                        {
                            strIsActive = "Active";
                        }
                        else
                        {
                            strIsActive = "InActive";
                        }

                        dr = dt.NewRow();

                        dr["strSno"] = strSno;
                        dr["strAppName"] = reApp.AppName;
                        dr["strConfig"] = strConfigName;
                        dr["strStoreName"] = strStoreName;
                        dr["strStoreEmail"] = strStoreEmail;
                        dr["strStoreContactNumber"] = strStoreContact;
                        dr["strUserName"] = strUserName;
                        dr["strUpdatedTime"] = strUpdatedTime;
                        dr["strIsUpdate"] = strIsUpdate;
                        dr["strIsActive"] = strIsActive;
                        dt.Rows.Add(dr);
                        //dr = dt.NewRow();

                        //Store the DataTable in ViewState
                        ViewState["CurrentTable"] = dt;
                    }
                }
            }            
            AppDB.Logger.Info("Fetching all the cmc configuration  from DB to the GridView");
            gdviewusers.DataSource = dt;
            gdviewusers.DataBind();

        }
        else
        {
            ddlcmc.Items.Clear();
            ddlcmc.Items.Add("Select");
        }
    }
    protected void ddlcmc_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlcmc.SelectedItem.Text != "Select")
        {
            GetCMCData();
        }
        else
        {
            ddlstorename.Items.Clear();
            ddlstorename.Items.Add("Select");
            GetAppData();
        }
    }

    private void GetCMCData()
    {
        string strStoreName, strStoreEmail, strStoreContact, strUserName, strIsActive, strIsUpdate, strIsUpdatedtime;
        long strSno;
        LOTServerDBSecurity.Models.RegisteredApplications reApp = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(ddlappname.SelectedItem.Text)).FirstOrDefault();
        LOTServerDBSecurity.Models.CMCConfiguration cmc = AppDB.securityDB.CMCConfig.ToList().Where(c => c.ConfigurationName.Equals(ddlcmc.SelectedItem.Text)).FirstOrDefault();
        if (cmc != null)
        {

            ddlstorename.DataSource = null;
            ddlstorename.Items.Clear();
            ddlstorename.Items.Add("Select");
            ddlstorename.DataTextField = "StoreName";
            ddlstorename.DataValueField = "SNo";
            ddlstorename.DataSource = AppDB.securityDB.StoreAdmin.Where(c => c.CMCConfig.ConfigurationName.Equals(ddlcmc.SelectedItem.Text)).ToList();
            ddlstorename.DataBind();
            lock (lockTarget)
            {
                dt = new DataTable();
                dr = null;

                dt.Columns.Add(new DataColumn("strSno", typeof(long)));
                dt.Columns.Add(new DataColumn("strAppName", typeof(string)));
                dt.Columns.Add(new DataColumn("strConfig", typeof(string)));
                dt.Columns.Add(new DataColumn("strStoreName", typeof(string)));
                dt.Columns.Add(new DataColumn("strStoreEmail", typeof(string)));
                dt.Columns.Add(new DataColumn("strStoreContactNumber", typeof(string)));
                dt.Columns.Add(new DataColumn("strUserName", typeof(string)));
                dt.Columns.Add(new DataColumn("strUpdatedTime", typeof(string)));
                dt.Columns.Add(new DataColumn("strIsUpdate", typeof(string)));
                dt.Columns.Add(new DataColumn("strIsActive", typeof(string)));
                List<LOTServerDBSecurity.Models.RegisteredApplications> lstReg = AppDB.securityDB.RegApps.ToList().Where(c => c.AppName.Equals(ddlappname.SelectedItem.Text)).ToList();
                List<LOTServerDBSecurity.Models.CMCConfiguration> lstcmc = reApp.LstCMCConfig.ToList();
                List<LOTServerDBSecurity.Models.StoreAdmin> lstStores = reApp.LstStoreAdmins.ToList();

                foreach (var item in cmc.RegApp.LstCMCConfig.Where(c=>c.ConfigurationName.Equals(ddlcmc.SelectedItem.Text)))
                {
                    string strConfigName = item.ConfigurationName;
                    string strFtpUserName = item.FtpUserName;
                    string strUpdatedTime = item.UpdatedTime.ToString();
                    string strAppName = reApp.AppName;
                    long Sno = item.SNo;
                    foreach (var item2 in item.RegApp.LstStoreAdmins.ToList().Where(c => c.CMCConfig.SNo.Equals(Sno)))
                    {
                        strSno = item2.SNo;
                        strStoreName = item2.StoreName;
                        strStoreEmail = item2.StoreEMail;
                        strStoreContact = item2.StoreContactNumber;
                        strUserName = item2.UserName;

                        strIsUpdatedtime = item2.UpdatedTime.ToString();
                        if (item2.IsUpdateAvailable)
                        {
                            strIsUpdate = "Enable";
                        }
                        else
                        {
                            strIsUpdate = "Disable";
                        }
                        if (item2.IsActive)
                        {
                            strIsActive = "Active";
                        }
                        else
                        {
                            strIsActive = "InActive";
                        }

                        dr = dt.NewRow();

                        dr["strSno"] = strSno;
                        dr["strAppName"] = reApp.AppName;
                        dr["strConfig"] = strConfigName;
                        dr["strStoreName"] = strStoreName;
                        dr["strStoreEmail"] = strStoreEmail;
                        dr["strStoreContactNumber"] = strStoreContact;
                        dr["strUserName"] = strUserName;
                        dr["strUpdatedTime"] = strUpdatedTime;
                        dr["strIsUpdate"] = strIsUpdate;
                        dr["strIsActive"] = strIsActive;
                        dt.Rows.Add(dr);
                        //dr = dt.NewRow();

                        //Store the DataTable in ViewState
                        ViewState["CurrentTable"] = dt;
                    }
                }
            }
            AppDB.Logger.Info("Fetching all the cmc configuration  from DB to the GridView");
            gdviewusers.DataSource = dt;
            gdviewusers.DataBind();

        }
        else
        {
            ddlcmc.Items.Clear();
            ddlcmc.Items.Add("Select");
        }
    }
    protected void ddlstorename_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlstorename.SelectedItem.Text != "Select")
        {
            GetStoreData();
        }
        else
        {
            GetCMCData();
        }
    }

    private void GetStoreData()
    {
        string strStoreName, strStoreEmail, strStoreContact, strUserName, strIsActive, strIsUpdate, strIsUpdatedtime;
        long strSno;
        LOTServerDBSecurity.Models.RegisteredApplications reApp = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(ddlappname.SelectedItem.Text)).FirstOrDefault();
        LOTServerDBSecurity.Models.CMCConfiguration cmc = AppDB.securityDB.CMCConfig.ToList().Where(c => c.ConfigurationName.Equals(ddlcmc.SelectedItem.Text)).FirstOrDefault();
        LOTServerDBSecurity.Models.StoreAdmin storeAdmin = AppDB.securityDB.StoreAdmin.ToList().Where(c => c.StoreName.Equals(ddlstorename.SelectedItem.Text)).FirstOrDefault();
        if (storeAdmin != null)
        {

            lock (lockTarget)
            {
                dt = new DataTable();
                dr = null;

                dt.Columns.Add(new DataColumn("strSno", typeof(long)));
                dt.Columns.Add(new DataColumn("strAppName", typeof(string)));
                dt.Columns.Add(new DataColumn("strConfig", typeof(string)));
                dt.Columns.Add(new DataColumn("strStoreName", typeof(string)));
                dt.Columns.Add(new DataColumn("strStoreEmail", typeof(string)));
                dt.Columns.Add(new DataColumn("strStoreContactNumber", typeof(string)));
                dt.Columns.Add(new DataColumn("strUserName", typeof(string)));
                dt.Columns.Add(new DataColumn("strUpdatedTime", typeof(string)));
                dt.Columns.Add(new DataColumn("strIsUpdate", typeof(string)));
                dt.Columns.Add(new DataColumn("strIsActive", typeof(string)));
                List<LOTServerDBSecurity.Models.RegisteredApplications> lstReg = AppDB.securityDB.RegApps.ToList().Where(c => c.AppName.Equals(ddlappname.SelectedItem.Text)).ToList();
                List<LOTServerDBSecurity.Models.CMCConfiguration> lstcmc = reApp.LstCMCConfig.ToList();
                List<LOTServerDBSecurity.Models.StoreAdmin> lstStores = reApp.LstStoreAdmins.ToList();

                foreach (var item2 in storeAdmin.RegApp.LstStoreAdmins.Where(c=>c.StoreName.Equals(ddlstorename.SelectedItem.Text)))
                {
                    strSno = item2.SNo;
                    strStoreName = item2.StoreName;
                    strStoreEmail = item2.StoreEMail;
                    strStoreContact = item2.StoreContactNumber;
                    strUserName = item2.UserName;

                    strIsUpdatedtime = item2.UpdatedTime.ToString();
                    if (item2.IsUpdateAvailable)
                    {
                        strIsUpdate = "Enable";
                    }
                    else
                    {
                        strIsUpdate = "Disable";
                    }
                    if (item2.IsActive)
                    {
                        strIsActive = "Active";
                    }
                    else
                    {
                        strIsActive = "InActive";
                    }

                 
                    foreach (var item in item2.RegApp.LstCMCConfig.ToList().Where(c => c.ConfigurationName.Equals(ddlcmc.SelectedItem.Text)))
                    {
                        string strConfigName = item.ConfigurationName;
                        string strFtpUserName = item.FtpUserName;
                        string strUpdatedTime = item.UpdatedTime.ToString();
                        string strAppName = reApp.AppName;
                        dr = dt.NewRow();

                        dr["strSno"] = strSno;
                        dr["strAppName"] = reApp.AppName;
                        dr["strConfig"] = strConfigName;
                        dr["strStoreName"] = strStoreName;
                        dr["strStoreEmail"] = strStoreEmail;
                        dr["strStoreContactNumber"] = strStoreContact;
                        dr["strUserName"] = strUserName;
                        dr["strUpdatedTime"] = strUpdatedTime;
                        dr["strIsUpdate"] = strIsUpdate;
                        dr["strIsActive"] = strIsActive;
                        dt.Rows.Add(dr);
                        //dr = dt.NewRow();

                        //Store the DataTable in ViewState
                        ViewState["CurrentTable"] = dt;
                    }
                }
            }
            AppDB.Logger.Info("Fetching all the cmc configuration  from DB to the GridView");
            gdviewusers.DataSource = dt;
            gdviewusers.DataBind();

        }
        else
        {
            ddlstorename.Items.Clear();
            ddlstorename.Items.Add("Select");
        }
    }
}