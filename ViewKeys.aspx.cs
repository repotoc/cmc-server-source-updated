﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ViewKeys : System.Web.UI.Page
{
    object lockTarget = new object();
    DataTable dt;
    string strIsUpdate, strIsActive;
    string ip = (string)System.Configuration.ConfigurationSettings.AppSettings["ip"];
    int port = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["port"]);
    protected void Page_Load(object sender, EventArgs e)
    {
        AppDB.securityDB = new LOTServerDBSecurity.DBFolder.LotServerDBSecurityContext();
        if (Session["username"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                lock (lockTarget)
                {
                    AppDB.Logger.Info("Initiating View Keys.");
                    //AppDB.Logger.Info("Checking for Application Name exists in DB... App Name: " + ddlappname.SelectedItem.Text);
                    AppDB.Logger.Info("Checking for Atleast One Security Key exists in DB..");
                    if (AppDB.securityDB.SecurityKey.ToList().Count > 0)
                    {
                        lock (lockTarget)
                        {
                            lock (lockTarget)
                            {
                                DataTable dt = new DataTable();
                                DataRow dr = null;

                                dt.Columns.Add(new DataColumn("strSno", typeof(long)));
                                dt.Columns.Add(new DataColumn("strAppName", typeof(string)));
                                dt.Columns.Add(new DataColumn("strConfig", typeof(string)));
                                dt.Columns.Add(new DataColumn("strStoreName", typeof(string)));
                                dt.Columns.Add(new DataColumn("strUserName", typeof(string)));
                                dt.Columns.Add(new DataColumn("strAuthCode", typeof(string)));
                                dt.Columns.Add(new DataColumn("strSysName", typeof(string)));
                                dt.Columns.Add(new DataColumn("strUpdatedTime", typeof(string)));
                                dt.Columns.Add(new DataColumn("strIsUpdate", typeof(string)));
                                dt.Columns.Add(new DataColumn("strIsActive", typeof(string)));
                                foreach (var item in AppDB.securityDB.SecurityKey.ToList())
                                {
                                    lock (lockTarget)
                                    {
                                        long strSno = item.SNo;
                                        LOTServerDBSecurity.Models.SecurityKey secKey = AppDB.securityDB.SecurityKey.Where(c => c.SNo.Equals(strSno)).FirstOrDefault();
                                        if (secKey != null)
                                        {
                                            string strAuthCode=secKey.AuthCode;
                                            string strSysName = secKey.SysName;
                                            if(secKey.IsUpdateAvailable)
                                            {
                                                strIsUpdate="Yes";
                                            }
                                            else
                                            {
                                                strIsUpdate="No";
                                            }

                                            if (secKey.IsEnabled)
                                            {
                                                strIsActive = "Enable";
                                            }
                                            else
                                            {
                                                strIsActive = "Disable";
                                            }
                                            if (AppDB.securityDB.StoreAdmin.ToList().Exists(c => c.SNo.Equals(secKey.StoreAdmin.SNo)))
                                            {
                                                LOTServerDBSecurity.Models.StoreAdmin strAdmin = AppDB.securityDB.StoreAdmin.Where(c => c.SNo.Equals(secKey.StoreAdmin.SNo)).FirstOrDefault();
                                                LOTServerDBSecurity.Models.CMCConfiguration cmc = AppDB.securityDB.CMCConfig.Where(c => c.SNo.Equals(strAdmin.CMCConfig.SNo)).FirstOrDefault();
                                                LOTServerDBSecurity.Models.RegisteredApplications regApp = AppDB.securityDB.RegApps.Where(c => c.SNo.Equals(cmc.RegApp.SNo)).FirstOrDefault();
                                                if (strAdmin != null && cmc != null && regApp != null)
                                                {
                                                    string strAppName = regApp.AppName;
                                                    string strcmc = cmc.ConfigurationName;
                                                    string strStoreName = strAdmin.StoreName;
                                                    string strUserName = strAdmin.UserName;
                                                    dr = dt.NewRow();
                                                    dr["strSno"] = strSno;
                                                    dr["strAppName"] = strAppName;
                                                    dr["strConfig"] = strcmc;
                                                    dr["strStoreName"] = strStoreName;
                                                    dr["strUserName"] = strUserName;
                                                    dr["strAuthCode"] = strAuthCode;
                                                    dr["strIsUpdate"] = strIsUpdate;
                                                    dr["strIsActive"] = strIsActive;
                                                    dr["strSysName"] = strSysName;
                                                    dt.Rows.Add(dr);
                                                    //Store the DataTable in ViewState
                                                    ViewState["CurrentTable"] = dt;
                                                }
                                            }
                                           
                                        }
                                    }
                                }
                                AppDB.Logger.Info("Fetching all the SecurityKeys from DataTable anf bind to the Gridview");
                                gdviewkey.DataSource = dt;
                                gdviewkey.DataBind();

                            }
                        }

                    }
                }
            }
        }
    }
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        Response.Redirect("KeyGeneratorPage.aspx");
    }
    protected void gdviewkey_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        #region Commented which help to get data
        if (e.CommandName == "Detail")
        {
            GridViewRow row = (GridViewRow)((Button)e.CommandSource).NamingContainer;
            lblid.Text = ((Label)row.FindControl("lblkey")).Text;
            lock (lockTarget)
            {
                long id = Convert.ToInt64(lblid.Text);
                LOTServerDBSecurity.Models.SecurityKey seckey = AppDB.securityDB.SecurityKey.ToList().Where(c => c.SNo.Equals(id)).FirstOrDefault();
                if (seckey != null)
                {
                    lblid.Text = seckey.SNo.ToString();
                    lblcustname.Text = seckey.Name;
                    lblcustemail.Text = seckey.CustEMail;
                    lblguid.Text = seckey.GUID;
                    lblhddinter.Text = seckey.HDDInterfaceType;
                    lblhddserial.Text = seckey.HDDSerial;
                    lblmediatype.Text = seckey.HDDMediaType;
                    lblsysname.Text = seckey.SysName;
                    lblauth.Text = seckey.AuthCode;
                    lblmobile.Text = seckey.ContactNumber;
                    lbldeviceid.Text = seckey.HDDDeviceType;
                    lblconftext.Text = seckey.StoreAdmin.RegApp.ConfidentialKey.SecretKey;
                    DetailPanel.Visible = true;
                }
            }
        }
        #endregion
    }
    protected void gdviewkey_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gdviewkey.EditIndex = e.NewEditIndex;
        gdviewkey.DataSource = ViewState["CurrentTable"];
        gdviewkey.DataBind();
    }
    protected void gdviewkey_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int indexrow = e.RowIndex;
        string strId = ((Label)gdviewkey.Rows[e.RowIndex].FindControl("lblkey")).Text;
        long id = Convert.ToInt64(strId);
        lock (lockTarget)
        {
            LOTServerDBSecurity.Models.SecurityKey secKeyUpdate = AppDB.securityDB.SecurityKey.Where(c => c.SNo.Equals(id)).FirstOrDefault();
            if (secKeyUpdate != null)
            {
                string strEnable = ((DropDownList)gdviewkey.Rows[e.RowIndex].FindControl("ddlenable")).SelectedItem.Text;
                string strUpdate = ((DropDownList)gdviewkey.Rows[e.RowIndex].FindControl("ddlupdate")).SelectedItem.Text;
                if (strEnable.Equals("Enable", StringComparison.OrdinalIgnoreCase))
                {
                    secKeyUpdate.IsEnabled = true;
                }
                else
                {
                    secKeyUpdate.IsEnabled = false;
                }
                if (strUpdate.Equals("Yes", StringComparison.OrdinalIgnoreCase))
                {
                    secKeyUpdate.IsUpdateAvailable = true;
                }
                else
                {
                    secKeyUpdate.IsUpdateAvailable = false;
                }
                AppDB.securityDB.SaveChanges();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Status Updated Successfully'); window.location='" + Request.ApplicationPath + "ViewKeys.aspx';", true);
                myfashionsServerSDK.RefreshAPI.RefreshWEBAPI refreshapi = new myfashionsServerSDK.RefreshAPI.RefreshWEBAPI(ip, port);
                refreshapi.RefreshDB();
                gdviewkey.EditIndex = -1;
                gdviewkey.DataSource = ViewState["CurrentTable"];
                gdviewkey.DataBind();
            }
        }
        
    }
    protected void gdviewkey_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gdviewkey.EditIndex = -1;
        gdviewkey.DataSource = ViewState["CurrentTable"];
        gdviewkey.DataBind();
    }
    protected void gdviewkey_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
    protected void imgClose_Click(object sender, ImageClickEventArgs e)
    {
        DetailPanel.Visible = false;
    }
    protected void gdviewkey_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dataTable = ViewState["CurrentTable"] as DataTable;
        if (dataTable != null)
        {
            DataView dataView = new DataView(dataTable);
            dataView.Sort = e.SortExpression;
            gdviewkey.DataSource = dataView;
            gdviewkey.DataBind();
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        lock (lockTarget)
        {
            long strId = Convert.ToInt64(lblid.Text);
            LOTServerDBSecurity.Models.SecurityKey secKey = AppDB.securityDB.SecurityKey.Where(c => c.SNo.Equals(strId)).FirstOrDefault();
            if (secKey != null)
            {
                AppDB.securityDB.SecurityKey.Remove(secKey);
                AppDB.securityDB.SaveChanges();
                myfashionsServerSDK.RefreshAPI.RefreshWEBAPI refreshapi = new myfashionsServerSDK.RefreshAPI.RefreshWEBAPI(ip, port);
                refreshapi.RefreshDB();
                AppDB.Logger.Info("Deleted Successfully.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Security Key Deleted Successfully'); window.location='" + Request.ApplicationPath + "ViewKeys.aspx';", true);
            }
        }
    }
}