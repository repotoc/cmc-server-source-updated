﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="ViewKeys.aspx.cs" Inherits="ViewKeys" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<script type="text/javascript">
		function Validate() {
			
				 var x = confirm("Are you sure you want to delete?");
				 if (x == true)
					 return true;
				 else {
					 if (document.getElementById("<%=Label1.ClientID%>") != null)
						 document.getElementById("<%=Label1.ClientID%>").innerText = "";
					 return false;
				 }
			
		 }
</script>
	  <script  type="text/javascript">
		  function SelectSingleRadiobutton(rdbtnid) {
			  var rdBtn = document.getElementById(rdbtnid);
			  var rdBtnList = document.getElementsByTagName("input");
			  for (i = 0; i < rdBtnList.length; i++) {
				  if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id) {
					  rdBtnList[i].checked = false;
				  }
			  }
		  }
</script>
	   <div class="MainContainer">
		   <asp:Button ID="btnAddNew" runat="server" Text="New Key" OnClick="btnAddNew_Click" CssClass="btn" />
		   <asp:Label ID="Label1" runat="server"></asp:Label>
		   <div class="grdDiv">
				 <asp:GridView ID="gdviewkey"  runat="server"  AutoGenerateColumns="False" OnSorting="gdviewkey_Sorting" OnRowEditing="gdviewkey_RowEditing" AllowSorting="true"   OnRowUpdating="gdviewkey_RowUpdating" OnRowCancelingEdit="gdviewkey_RowCancelingEdit" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" OnRowCommand="gdviewkey_RowCommand"   BorderWidth="1px" CellPadding="3" CellSpacing="2" >
		<Columns>
		
			<asp:TemplateField HeaderText="SNo" Visible="false">
				<ItemTemplate>
					<asp:Label ID="lblkey" runat="server" Text='<%#Eval("strSNo") %>' ></asp:Label>
				</ItemTemplate>
			</asp:TemplateField>
			  <asp:TemplateField HeaderText="ApplicationName" SortExpression="strAppName">
				<ItemTemplate>
					<asp:Label ID="lblappname" runat="server" Text='<%#Eval("strAppName") %>'></asp:Label>
				</ItemTemplate>
				  <EditItemTemplate>
					  <asp:Label ID="lblappname" runat="server" Enabled="false" Text='<%#Eval("strAppName") %>'></asp:Label>
				  </EditItemTemplate>
			</asp:TemplateField>
			  <asp:TemplateField HeaderText="CMC Name" SortExpression="strConfig">
				<ItemTemplate>
					<asp:Label ID="lblcmcconfig" runat="server" Text='<%#Eval("strConfig") %>'></asp:Label>
				</ItemTemplate>
			</asp:TemplateField>
			 <asp:TemplateField HeaderText="Store Name" SortExpression="strStoreName">
				<ItemTemplate>
					<asp:Label ID="lblStoreName" runat="server" Text='<%#Eval("strStoreName") %>'></asp:Label>
				</ItemTemplate>
				 <EditItemTemplate>
					 <asp:Label ID="lblStoreName" runat="server" Enabled="false" Text='<%#Eval("strStoreName") %>'></asp:Label>
				 </EditItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="User Name" SortExpression="strUserName">
				<ItemTemplate>
					<asp:Label ID="lblUserName" runat="server" Text='<%#Eval("strUserName") %>'></asp:Label>
				</ItemTemplate>
				<EditItemTemplate>
					<asp:Label ID="lblUserName" runat="server" Enabled="false" Text='<%#Eval("strUserName") %>'></asp:Label>
				</EditItemTemplate>
			</asp:TemplateField>
				 <asp:TemplateField HeaderText="System Name" SortExpression="strSysName">
				<ItemTemplate>
					<asp:Label ID="lblSysName" runat="server" Text='<%#Eval("strSysName") %>'></asp:Label>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Authorization Code" SortExpression="strAuthCode">
				<ItemTemplate>
					<asp:Label ID="lblauthcode" runat="server" Text='<%#Eval("strAuthCode") %>'></asp:Label>
				</ItemTemplate>
				<EditItemTemplate>
					<asp:Label ID="lblauthcode" runat="server" Enabled="false" Text='<%#Eval("strAuthCode") %>'></asp:Label>
				</EditItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField  HeaderText="Service Status" SortExpression="strIsActive">
				<ItemTemplate>
					<asp:Label ID="lblenable" runat="server" Text='<%#Eval("strIsActive") %>'></asp:Label>
				</ItemTemplate>
				<EditItemTemplate>
					<asp:DropDownList ID="ddlenable" runat="server" >
						<asp:ListItem>Enable</asp:ListItem>
						<asp:ListItem>Disable</asp:ListItem>
					</asp:DropDownList>
				</EditItemTemplate>
			</asp:TemplateField>
			 <asp:TemplateField HeaderText="Update Status" SortExpression="strIsUpdate">
				<ItemTemplate>
					<asp:Label ID="lblupdate" runat="server" Text='<%#Eval("strIsUpdate") %>'></asp:Label>
				</ItemTemplate>
				 <EditItemTemplate>
					<asp:DropDownList ID="ddlupdate" runat="server" >
						<asp:ListItem>Yes</asp:ListItem>
						<asp:ListItem>No</asp:ListItem>
					</asp:DropDownList>
				</EditItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField  ShowHeader="False">
					<EditItemTemplate>
						<asp:LinkButton ID="LnkBtnUpdate" runat="server" CausesValidation="True"
							CommandName="Update" Text="Update"></asp:LinkButton>
						&nbsp;<asp:LinkButton ID="LnkBtnCancel" runat="server" CausesValidation="False"
							CommandName="Cancel" Text="Cancel"></asp:LinkButton>
					</EditItemTemplate>
					<ItemTemplate>
						<asp:LinkButton ID="LnkBtnEdit" runat="server" CausesValidation="False"
							CommandName="Edit" Text="Edit"></asp:LinkButton>
					</ItemTemplate>
				</asp:TemplateField>
			<asp:TemplateField  HeaderText="View">
				<ItemTemplate>
					<asp:Button ID="BtnView" runat="server"  CommandName="Detail"  Text="View"/>
				</ItemTemplate>
			</asp:TemplateField>

		</Columns>
					<FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
					<HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
					<PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
					<RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
					<SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
					<SortedAscendingCellStyle BackColor="#FFF1D4" />
					<SortedAscendingHeaderStyle BackColor="#B95C30" />
					<SortedDescendingCellStyle BackColor="#F1E5CE" />
					<SortedDescendingHeaderStyle BackColor="#93451F" />
	</asp:GridView>
			   </div>
		   <div id="DetailPanel" class="PopUp" visible="false" runat="server">
			<table style="width:50%; height:50%; background:#FFF;
			margin: 0 auto;
			margin-top: 9%;">
				<tr>
					<th colspan="2">Key Generator
					</th>
					 <th>
						<asp:ImageButton ID="imgClose" runat="server" ImageUrl="~/images/close.png" OnClick="imgClose_Click" Height="30px" Width="30px"/>
					</th>
				</tr>
				<tr>
					<td style="width: 45%">SNo:
					</td>
					<td>
						<asp:Label ID="lblid" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td>Customer Name:
					</td>
					<td>
						<asp:Label ID="lblcustname" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td>System  Name:
					</td>
					<td>
						<asp:Label ID="lblsysname" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td>Customer Email:
					</td>
					<td>
						<asp:Label ID="lblcustemail" runat="server"></asp:Label>
					</td>
				</tr>
				  <tr>
					<td>Authorization Code
					</td>
					<td>
						<asp:Label ID="lblauth" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td>GUID:
					</td>
					<td>
						<asp:Label ID="lblguid" runat="server"></asp:Label>
					</td>

				</tr>
				<tr>
					<td>HDD Serial:
					</td>
					<td>
						<asp:Label ID="lblhddserial" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td>Device ID:
					</td>
					<td>
						<asp:Label ID="lbldeviceid" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td>HDD Inter Type:
					</td>
					<td>
						<asp:Label ID="lblhddinter" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td>Mobile No:
					</td>
					<td>
						<asp:Label ID="lblmobile" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td>Confidential Text:
					</td>
					<td>
						<asp:Label ID="lblconftext" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td>Media Type:
					</td>
					<td>
						<asp:Label ID="lblmediatype" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<asp:Button ID="btnDelete" runat="server" Text="Delete" OnClientClick="return Validate();" OnClick="btnDelete_Click" />
					</td>
				</tr>
			</table>
		</div>
		   <%--<div id="EditPanel" class="PopUp" visible="false" runat="server">
			<table style="width:50%; height:50%; background:#FFF;
			margin: 0 auto;
			margin-top: 9%;">
				<tr>
					<th colspan="2">Key Generator
					</th>
				</tr>
				<tr>
					<td style="width: 45%">SNo:
					</td>
					<td>
						<asp:Label ID="lblid" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td>Customer Name:
					</td>
					<td>
						<asp:Label ID="lblcustname" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td>Is Update Available:
					</td>
					<td>
						<asp:Label ID="lblupdatedtime" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<asp:CheckBox ID="chkupdateavail" runat="server" Text="Is UpdateAvailable" />
					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align:center;">
						<asp:Button ID="btnUpdate" CommandName="Update" runat="server" Text="Update" OnClick="btnUpdate_Click" CausesValidation="false" />
				   
						<asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false" OnClick="btnCancel_Click" />
					</td>
				</tr>
			</table>
		</div>--%>

		   </div>
	
</asp:Content>

