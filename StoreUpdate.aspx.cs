﻿using LOTServerDBSecurity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class StoreUpdate : System.Web.UI.Page
{
    object lockTarget = new object();
    string ip = (string)System.Configuration.ConfigurationSettings.AppSettings["ip"];
    int port = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["port"]);
    protected void Page_Load(object sender, EventArgs e)
    {
        AppDB.securityDB = new LOTServerDBSecurity.DBFolder.LotServerDBSecurityContext();
        if (Session["username"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                lock (lockTarget)
                {
                    AppDB.Logger.Info("Initiating Create Store Update.");
                    //AppDB.Logger.Info("Checking for Application Name exists in DB... App Name: " + ddlappname.SelectedItem.Text);
                    AppDB.Logger.Info("Checking for Atleast One Application Name exists in DB..");
                    if (AppDB.securityDB.RegApps.ToList().Count > 0)
                    {
                        lock (lockTarget)
                        {
                            AppDB.Logger.Info("Fetching all the applications and bind to the dropdownlist");
                            ddlappname.DataSource = AppDB.securityDB.RegApps.ToList();
                            ddlappname.DataBind();
                        }
                    }
                }
            }
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        AppDB.Logger.Info("Initiating Submit Button in StoreUpdate");
        LOTServerDBSecurity.Models.RegisteredApplications regApp = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(ddlappname.SelectedItem.Text)).FirstOrDefault();
        if (regApp != null)
        {
            if (regApp.IsUpdateAvailable)
            {
                if (chkupdates.Checked == true)
                {
                    AppDB.Logger.Info("Fetching Data when Checkbox Checked is True");
                    lock (lockTarget)
                    {
                        if (ddlstores.SelectedItem.Text != "Select")
                        {
                            AppDB.Logger.Info("Fetching the storeadmin according to the selected store.. ddlstores" + ddlstores.SelectedItem.Text + " ");
                            lock (lockTarget)
                            {
                                LOTServerDBSecurity.Models.StoreAdmin StoreAdmin = AppDB.securityDB.StoreAdmin.Where(c => c.StoreName.Equals(ddlstores.SelectedItem.Text)).FirstOrDefault();
                                StoreAdmin.IsUpdateAvailable = true;


                                AppDB.Logger.Info("Fetching all the security keys according to the selected store.. Security Key" + StoreAdmin.StoreName + " ");
                                lock (lockTarget)
                                {
                                    AppDB.securityDB.SecurityKey.Where(c => c.StoreAdmin.UserName.Equals(StoreAdmin.StoreName)).ToList().ForEach(c =>
                                    {
                                        c.IsUpdateAvailable = true;
                                    });
                                    AppDB.securityDB.SaveChanges();
                                }
                             
                                myfashionsServerSDK.RefreshAPI.RefreshWEBAPI refreshapi = new myfashionsServerSDK.RefreshAPI.RefreshWEBAPI(ip, port);
                                refreshapi.RefreshDB();
                            }
                            AppDB.Logger.Info("Store Update Saved Successfully");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('StoreUpdate Updated Succesffully'); window.location='" + Request.ApplicationPath + "StoreUpdate.aspx';", true);
                        }
                        else
                        {
                            AppDB.Logger.Info("Required fields should not be empty.");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Required fields should not be empty.');", true);
                        }

                    }
                }
                else
                {
                    AppDB.Logger.Info("Fetching Data when Checkbox Checked is False");
                    lock (lockTarget)
                    {
                        if (ddlstores.SelectedItem.Text != "Select")
                        {
                            AppDB.Logger.Info("Fetching the storeadmin according to the selected store.. ddlstores" + ddlstores.SelectedItem.Text + " ");
                            lock (lockTarget)
                            {
                                LOTServerDBSecurity.Models.StoreAdmin StoreAdmin = AppDB.securityDB.StoreAdmin.Where(c => c.StoreName.Equals(ddlstores.SelectedItem.Text)).FirstOrDefault();

                                StoreAdmin.IsUpdateAvailable = false;
                                AppDB.Logger.Info("Fetching all the security keys according to the selected store.. Security Key" + StoreAdmin.StoreName + " ");
                                lock (lockTarget)
                                {
                                    AppDB.securityDB.SecurityKey.Where(c => c.StoreAdmin.StoreName.Equals(StoreAdmin.StoreName)).ToList().ForEach(c =>
                                    {
                                        c.IsUpdateAvailable = false;
                                    });
                                    AppDB.securityDB.SaveChanges();

                                }
                                myfashionsServerSDK.RefreshAPI.RefreshWEBAPI refreshapi = new myfashionsServerSDK.RefreshAPI.RefreshWEBAPI(ip, port);
                                refreshapi.RefreshDB();
                            }

                            AppDB.Logger.Info("Store Service Saved Successfully");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Store Service Updated Succesffully'); window.location='" + Request.ApplicationPath + "StoreUpdate.aspx';", true);
                        }
                        else
                        {
                            AppDB.Logger.Info("Required fields should not be empty.");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Required fields should not be empty.');", true);
                        }
                    }
                }
            }
            else
            {
                AppDB.Logger.Info("App Update status is Inactive.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('App Update status is Inactive.');", true);
            }
        }
        
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Home.aspx");
    }
    protected void ddlappname_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlappname.SelectedItem.Text != "Select")
        {
            lock (lockTarget)
            {
                AppDB.Logger.Info("Fetching the Registered Application from DB according to the selected.. ddlappname"+ddlappname.SelectedItem.Text+"");
                RegisteredApplications selectedRegApp = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(ddlappname.SelectedItem.Text.Trim(), StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                if (selectedRegApp != null)
                {
                    lock (lockTarget)
                    {
                        AppDB.Logger.Info("Fetching the StorAdmins  data from DB according to the selected application and bind to the DropDownList");
                        ddlstores.Items.Clear();
                        ddlstores.Items.Add("Select");
                        ddlstores.DataSource = selectedRegApp.LstStoreAdmins.ToList();
                        ddlstores.DataBind();
                    }
                }
            }
        }
        else
        {
            ddlstores.Items.Clear();
            ddlstores.Items.Add("Select");
        }
    }
    protected void ddlstores_SelectedIndexChanged1(object sender, EventArgs e)
    {
        if (ddlstores.SelectedItem.Text != "Select")
        {
            lock (lockTarget)
            {
                LOTServerDBSecurity.Models.StoreAdmin strAdmin = AppDB.securityDB.StoreAdmin.Where(c => c.StoreName.Equals(ddlstores.SelectedItem.Text)).FirstOrDefault();
                if (strAdmin != null)
                {
                    if (strAdmin.IsUpdateAvailable == true)
                    {
                        chkupdates.Checked = true;
                    }
                    else
                    {
                        chkupdates.Checked = false;
                    }
                }
            }
        }
        else
        {
            AppDB.Logger.Info("Required fields should not be empty.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Required fields should not be empty.');", true);
        }
    }
}