﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="RegisterApp.aspx.cs" Inherits="RegisterApp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <style>
         .table > tbody > tr > td {border:none;
            }
    </style>
      <table class="table"  style="width:50%;margin:62px 22px 0px 280px;">
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
          <tr>
            <td>Application Name</td>
            <td><asp:TextBox ID="txtAppName" runat="server" CssClass="textboxstyle" ></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtAppName" ForeColor="Red" Font-Size="Smaller" ErrorMessage="App Name should accept  Alphanumerics only" ValidationExpression="^[0-9a-zA-Z ]+$" ></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "txtAppName" ID="RegularExpressionValidator3" ForeColor="Red" Font-Size="Smaller" ValidationExpression = "^[\s\S]{3,}$" runat="server" ErrorMessage="Minimum 3  characters required."></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "txtAppName" ID="RegularExpressionValidator4" ForeColor="Red" Font-Size="Smaller" ValidationExpression ="^[\s\S]{0,50}$" runat="server" ErrorMessage="Limit Should Not Exceed 50 characters."></asp:RegularExpressionValidator>
                <br />
            </td>
        </tr>
           <tr> 
            <td>Version Number</td>
            <td><asp:TextBox ID="txtversion" runat="server" CssClass="textboxstyle" ></asp:TextBox>
                 <br />
                <asp:RegularExpressionValidator ID="rev1" runat="server" ControlToValidate="txtversion" ForeColor="Red" Font-Size="Smaller" ErrorMessage="Version Should Be Only Alphanumerics only" ValidationExpression="^[0-9a-zA-Z.]+$" ></asp:RegularExpressionValidator>
                 <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "txtversion" ID="RegularExpressionValidator5" ForeColor="Red" Font-Size="Smaller" ValidationExpression = "^[\s\S]{1,}$" runat="server" ErrorMessage="Minimum 1  characters required."></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "txtversion" ID="RegularExpressionValidator6" ForeColor="Red" Font-Size="Smaller" ValidationExpression ="^[\s\S]{0,8}$" runat="server" ErrorMessage="Limit Should Not Exceed 8 characters."></asp:RegularExpressionValidator>
                <br />
            </td>
        </tr>
           <tr>
            <td colspan="2">
                <asp:Label ID="lblupdateavail" runat="server" Text="Update Status" Visible="false"></asp:Label>
                <asp:RadioButtonList ID="radList" runat="server" Width="300px"  RepeatColumns="2" Visible="false" RepeatDirection="Horizontal" Height="20px" >
                    <asp:ListItem>Active</asp:ListItem>
                    <asp:ListItem>Inactive</asp:ListItem>
                </asp:RadioButtonList>
               
            </td>
        </tr>
          <tr>
            
            <td colspan="2" style="text-align:center">
                <asp:Button ID="btnSubmit" runat="server"  Text="Submit"  CssClass="btn"   OnClick="btnSubmit_Click"/>
                <asp:Button ID="btnCancel" runat="server"  Text="Cancel"  CssClass="btn" CausesValidation="false"   OnClick="btnCancel_Click"/>
            </td>
        </tr>
          </table>
</asp:Content>

