﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="AppUpdate.aspx.cs" Inherits="AppUpdate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <style>
         .table > tbody > tr > td {border:none;
            }
    </style>
      <table class="table"  style="width:50%;margin:62px 22px 0px 280px;">
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
           <tr>
            <td>Select Application Name</td>
            <td><asp:DropDownList ID="ddlappname" runat="server"  DataTextField="AppName" DataValueField="SNo"  AutoPostBack="true"   AppendDataBoundItems="true" CssClass="dropdown" OnSelectedIndexChanged="ddlappname_SelectedIndexChanged">
                <asp:ListItem Value="-1" Text="Select"></asp:ListItem>
                </asp:DropDownList>
                <br />
            </td>
        </tr>
          <tr>
            <td colspan="2"><asp:CheckBox ID="chkupdates" runat="server" Text="Updates Available" />
                <br />
            </td>
        </tr>
          <tr>
            
            <td colspan="2" style="text-align:center">
                <asp:Button ID="btnSubmit" runat="server"  Text="Submit"  CssClass="btn"   OnClick="btnSubmit_Click"/>
                <asp:Button ID="btnCancel" runat="server"  Text="Cancel"  CssClass="btn" CausesValidation="false" OnClick="btnCancel_Click"/>
            </td>
        </tr>
          </table>
</asp:Content>

