﻿using LOTServerDBSecurity.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ViewCMC : System.Web.UI.Page
{
    object lockTarget = new object();
    DataTable dt, dtSearch;
    DataRow dr, dr1;
    string ip = (string)System.Configuration.ConfigurationSettings.AppSettings["ip"];
    int port = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["port"]);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["username"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                lock (lockTarget)
                {
                    AppDB.Logger.Info("Initiating View CMC.");
                    //AppDB.Logger.Info("Checking for Application Name exists in DB... App Name: " + ddlappname.SelectedItem.Text);
                    AppDB.Logger.Info("Checking for Atleast One Configuration exists in DB..");
                    if (AppDB.securityDB.CMCConfig.ToList().Count > 0)
                    {
                        GetTotalData();
                        ddlappname.DataSource = AppDB.securityDB.RegApps.ToList();
                        ddlappname.DataBind();
                    }
                }
            }
        }
    }

    private void GetTotalData()
    {
        lock (lockTarget)
        {
             dt = new DataTable();
            dr = null;
            dt.Columns.Add(new DataColumn("strSno", typeof(long)));
            dt.Columns.Add(new DataColumn("strAppName", typeof(string)));
            dt.Columns.Add(new DataColumn("strConfigName", typeof(string)));
            dt.Columns.Add(new DataColumn("strFtpUserName", typeof(string)));
            dt.Columns.Add(new DataColumn("strUpdatedTime", typeof(string)));
            foreach (var item in AppDB.securityDB.CMCConfig.ToList())
            {
                long strSno = item.SNo;
                LOTServerDBSecurity.Models.CMCConfiguration cmcconfig = AppDB.securityDB.CMCConfig.Where(c => c.SNo.Equals(strSno)).FirstOrDefault();
                if (cmcconfig != null)
                {
                    string strConfigName = cmcconfig.ConfigurationName;
                    string strFtpUserName = cmcconfig.FtpUserName;
                    string strUpdatedTime = cmcconfig.UpdatedTime.ToString();
                    if (AppDB.securityDB.RegApps.ToList().Exists(c => c.SNo.Equals(cmcconfig.RegApp.SNo)))
                    {
                        LOTServerDBSecurity.Models.RegisteredApplications regapp = AppDB.securityDB.RegApps.Where(c => c.SNo.Equals(cmcconfig.RegApp.SNo)).FirstOrDefault();
                        string strAppName = regapp.AppName;
                        dr = dt.NewRow();

                        dr["strSno"] = strSno;
                        dr["strAppName"] = strAppName;
                        dr["strConfigName"] = strConfigName;
                        dr["strFtpUserName"] = strFtpUserName;
                        dr["strUpdatedTime"] = strUpdatedTime;
                        dt.Rows.Add(dr);
                        //dr = dt.NewRow();

                        //Store the DataTable in ViewState
                        ViewState["CurrentTable"] = dt;
                    }

                }
            }
            AppDB.Logger.Info("Fetching all the cmc configuration  from DB to the GridView");
            gdviewcmc.DataSource = dt;
            gdviewcmc.DataBind();

        }
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        AppDB.Logger.Info("Initiating Application Configuration ID: " + lblid.Text+ " Configuration Name: " +txtConfigName.Text);
        if (!string.IsNullOrEmpty(txtConfigName.Text) && !string.IsNullOrEmpty(txtclientlocpath.Text) && !string.IsNullOrEmpty(txtftppwd.Text) && !string.IsNullOrEmpty(txtftppath.Text) && !string.IsNullOrEmpty(txtftpusername.Text))
        {
            lock (lockTarget)
            {
                AppDB.Logger.Info("Application Config validating completed.");
                long id = Convert.ToInt64(lblid.Text);
                LOTServerDBSecurity.Models.CMCConfiguration updateConfig = AppDB.securityDB.CMCConfig.Where(c => c.SNo.Equals(id)).FirstOrDefault();
                if (updateConfig != null)
                {
                    if (AppDB.securityDB.RegApps.ToList().Exists(c => c.AppName.Equals(txtAppName.Text)))
                    {
                        if (AppDB.securityDB.CMCConfig.ToList().Exists(c => c.ConfigurationName.Equals(txtConfigName.Text, StringComparison.OrdinalIgnoreCase)))
                        {
                            try
                            {
                                updateConfig.FtpPassword = txtftppwd.Text.Trim();
                                updateConfig.FtpPath = txtftppath.Text.Trim();
                                updateConfig.FtpUserName = txtftpusername.Text.Trim();
                                updateConfig.ClientPath = txtclientlocpath.Text.Trim();
                                updateConfig.ConfigurationName = txtConfigName.Text.Trim();
                                updateConfig.LaunchAppPath = txtlaunchapppath.Text.Trim();
                                lock (lockTarget)
                                {
                                    updateConfig.RegApp = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(txtAppName.Text)).FirstOrDefault();
                                }
                                updateConfig.UpdatedTime = DateTime.Now;
                                AppDB.securityDB.SaveChanges();
                                myfashionsServerSDK.RefreshAPI.RefreshWEBAPI refreshapi = new myfashionsServerSDK.RefreshAPI.RefreshWEBAPI(ip, port);
                                refreshapi.RefreshDB();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Configuration Updated Successfully'); window.location='" + Request.ApplicationPath + "ViewCMC.aspx';", true);
                                clearAll();
                                EditPanel.Visible = false;
                            }
                            catch (Exception ex)
                            {
                                AppDB.Logger.ErrorException(ex.Message, ex);
                            }
                        }
                       
                    }
                    else
                    {
                        AppDB.Logger.Info("Invalid Registered Application.");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Invalid Registered Application.');", true);
                    }
                }
                else
                {
                    AppDB.Logger.Info("Configuration id not found.");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Configuration id not found.');", true);
                }
            }
        }
        else
        {
            AppDB.Logger.Info("Invalid Configuration.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Invalid Configuration.');", true);
        }
        
    }
    protected void btnNew_Click(object sender, EventArgs e)
    {
        Response.Redirect("CMCConfiguration.aspx");
    }
   
    protected void gdviewcmc_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        lock (lockTarget)
        {
            long id = Convert.ToInt64(lblid.Text);
            LOTServerDBSecurity.Models.CMCConfiguration cmc = AppDB.securityDB.CMCConfig.Where(c => c.SNo.Equals(id)).FirstOrDefault();
            txtlaunchapppath.Text = cmc.LaunchAppPath;
            txtclientlocpath.Text = cmc.ClientPath;
            txtConfigName.Text = cmc.ConfigurationName;
            txtftppath.Text = cmc.FtpPath;
            txtftppwd.Text = cmc.FtpPassword;
            txtftpusername.Text = cmc.FtpUserName;
            txtAppName.Text = cmc.RegApp.AppName;
        }
        
    }

    private void clearAll()
    {
        txtclientlocpath.Text = "";
        txtConfigName.Text="";
        txtftppath.Text="";
        txtftppwd.Text="";
        txtftpusername.Text="";
        txtlaunchapppath.Text="";
    }
    protected void imgClose_Click(object sender, ImageClickEventArgs e)
    {
        EditPanel.Visible = false;
    }
    protected void btnEdit_Click(object sender, EventArgs e)
    {
        Label1.Visible = false;
        bool isChecked = false;
        foreach (GridViewRow row in gdviewcmc.Rows)
        {
            RadioButton rb = (RadioButton)row.FindControl("RadioButton1");
            if (rb.Checked)
            {
                isChecked = true;
                EditPanel.Visible = true;
            }
        }

        if (!isChecked)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Please Select AnyOne.');", true);
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        Label1.Visible = false;
        long id = 0;
        int Index;
        DataTable dt = (DataTable)ViewState["CurrentTable"];

        for (int i = 0; i < gdviewcmc.Rows.Count; i++)
        {
            GridViewRow row = gdviewcmc.Rows[i];
            RadioButton rb = (RadioButton)row.FindControl("RadioButton1");
            if (rb.Checked)
            {
                Index = row.RowIndex;
                //int strI = dt.Rows.IndexOf(dt.Rows[Index]);
                string strId = dt.Rows[Index]["strSno"].ToString();
                try
                {

                    id = Convert.ToInt64(strId);
                    AppDB.Logger.Info("Initiating Delete Configuration ID: " + id);
                    LOTServerDBSecurity.Models.CMCConfiguration updateConfig = AppDB.securityDB.CMCConfig.Where(c => c.SNo.Equals(id)).FirstOrDefault();
                    if (updateConfig != null)
                    {
                        List<LOTServerDBSecurity.Models.StoreAdmin> storeAdmin=AppDB.securityDB.StoreAdmin.Where(c => c.CMCConfig.ConfigurationName.Equals(updateConfig.ConfigurationName)).ToList();

                        if (storeAdmin == null)
                        {
                            AppDB.securityDB.CMCConfig.Remove(updateConfig);
                            AppDB.securityDB.SaveChanges();
                            myfashionsServerSDK.RefreshAPI.RefreshWEBAPI refreshapi = new myfashionsServerSDK.RefreshAPI.RefreshWEBAPI(ip, port);
                            refreshapi.RefreshDB();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('CMC Configuration Deleted Successfully'); window.location='" + Request.ApplicationPath + "ViewCMC.aspx';", true);
                        }
                        else if (storeAdmin.Count == 0)
                        {
                            AppDB.securityDB.CMCConfig.Remove(updateConfig);
                            AppDB.securityDB.SaveChanges();
                            myfashionsServerSDK.RefreshAPI.RefreshWEBAPI refreshapi = new myfashionsServerSDK.RefreshAPI.RefreshWEBAPI(ip, port);
                            refreshapi.RefreshDB();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('CMC Configuration Deleted Successfully'); window.location='" + Request.ApplicationPath + "ViewCMC.aspx';", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('First Delete Users related to this CMC Configuration.');", true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    AppDB.Logger.ErrorException(ex.Message, ex);
                }
            }
        }
    }
    protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
    {
        int Index;
        DataTable dt = (DataTable)ViewState["CurrentTable"];

        for (int i = 0; i < gdviewcmc.Rows.Count; i++)
        {
            GridViewRow row = gdviewcmc.Rows[i];
            RadioButton rdb = (RadioButton)row.FindControl("RadioButton1");
            if (rdb.Checked == true)
            {
                lock (lockTarget)
                {
                    Index = row.RowIndex;
                    lblid.Text = dt.Rows[Index]["strSno"].ToString();
                    long id = Convert.ToInt64(lblid.Text);
                    LOTServerDBSecurity.Models.CMCConfiguration cmc = AppDB.securityDB.CMCConfig.Where(c => c.SNo.Equals(id)).FirstOrDefault();
                    txtlaunchapppath.Text = cmc.LaunchAppPath;
                    txtclientlocpath.Text = cmc.ClientPath;
                    txtConfigName.Text = cmc.ConfigurationName;
                    txtftppath.Text = cmc.FtpPath;
                    txtftppwd.Text = cmc.FtpPassword;
                    txtftpusername.Text = cmc.FtpUserName;
                    txtAppName.Text = cmc.RegApp.AppName;
                }
            }
        }
    }
    protected void gdviewcmc_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dataTable = ViewState["CurrentTable"] as DataTable;
        if (dataTable != null)
        {
            DataView dataView = new DataView(dataTable);
            dataView.Sort = e.SortExpression;
            gdviewcmc.DataSource = dataView;
            gdviewcmc.DataBind();
        }
    }
    protected void ddlappname_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlappname.SelectedItem.Text != "Select")
        {
            GetAppData();
        }
        else
        {
            ddlcmc.Items.Clear();
            ddlcmc.Items.Add("Select");
            GetTotalData();
        }
    }

    private void GetAppData()
    {
        LOTServerDBSecurity.Models.RegisteredApplications reApp = AppDB.securityDB.RegApps.ToList().Where(c => c.AppName.Equals(ddlappname.SelectedItem.Text)).FirstOrDefault();
        if (reApp != null)
        {
            ddlcmc.DataSource = null;
            ddlcmc.Items.Clear();
            ddlcmc.Items.Add("Select");
            ddlcmc.DataTextField = "ConfigurationName";
            ddlcmc.DataValueField = "SNo";
            ddlcmc.DataSource = reApp.LstCMCConfig.ToList();
            ddlcmc.DataBind();
            lock (lockTarget)
            {
                dt = new DataTable();
                dr = null;
                dt.Columns.Add(new DataColumn("strSno", typeof(long)));
                dt.Columns.Add(new DataColumn("strAppName", typeof(string)));
                dt.Columns.Add(new DataColumn("strConfigName", typeof(string)));
                dt.Columns.Add(new DataColumn("strFtpUserName", typeof(string)));
                dt.Columns.Add(new DataColumn("strUpdatedTime", typeof(string)));

                List<LOTServerDBSecurity.Models.CMCConfiguration> lstcmc = reApp.LstCMCConfig.ToList();
                foreach (var item1 in lstcmc)
                {
                    string strConfigName = item1.ConfigurationName;
                    string strFtpUserName = item1.FtpUserName;
                    string strUpdatedTime = item1.UpdatedTime.ToString();
                    string strAppName = reApp.AppName;
                    long strSno = item1.SNo;
                    dr = dt.NewRow();

                    dr["strSno"] = strSno;
                    dr["strAppName"] = strAppName;
                    dr["strConfigName"] = strConfigName;
                    dr["strFtpUserName"] = strFtpUserName;
                    dr["strUpdatedTime"] = strUpdatedTime;
                    dt.Rows.Add(dr);
                    //dr = dt.NewRow();

                    //Store the DataTable in ViewState
                    ViewState["CurrentTable"] = dt;
                }


            }
            AppDB.Logger.Info("Fetching all the cmc configuration  from DB to the GridView");
            gdviewcmc.DataSource = dt;
            gdviewcmc.DataBind();

        }
        else
        {
            ddlcmc.Items.Clear();
            ddlcmc.Items.Add("Select");
        }
    }
    protected void ddlcmc_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlcmc.SelectedItem.Text != "Select")
        {
            lock (lockTarget)
            {
                dtSearch = new DataTable();
                dr1 = null;
                dtSearch.Columns.Add(new DataColumn("strSno", typeof(long)));
                dtSearch.Columns.Add(new DataColumn("strAppName", typeof(string)));
                dtSearch.Columns.Add(new DataColumn("strConfigName", typeof(string)));
                dtSearch.Columns.Add(new DataColumn("strFtpUserName", typeof(string)));
                dtSearch.Columns.Add(new DataColumn("strUpdatedTime", typeof(string)));
              
                    LOTServerDBSecurity.Models.CMCConfiguration cmcconfig = AppDB.securityDB.CMCConfig.Where(c => c.ConfigurationName.Equals(ddlcmc.SelectedItem.Text)).FirstOrDefault();
                    if (cmcconfig != null)
                    {
                        long strSno = cmcconfig.SNo;
                        string strConfigName = cmcconfig.ConfigurationName;
                        string strFtpUserName = cmcconfig.FtpUserName;
                        string strUpdatedTime = cmcconfig.UpdatedTime.ToString();
                        if (AppDB.securityDB.RegApps.ToList().Exists(c => c.SNo.Equals(cmcconfig.RegApp.SNo)))
                        {
                            LOTServerDBSecurity.Models.RegisteredApplications regapp = AppDB.securityDB.RegApps.Where(c => c.SNo.Equals(cmcconfig.RegApp.SNo)).FirstOrDefault();
                            string strAppName = regapp.AppName;
                            dr1 = dtSearch.NewRow();

                            dr1["strSno"] = strSno;
                            dr1["strAppName"] = strAppName;
                            dr1["strConfigName"] = strConfigName;
                            dr1["strFtpUserName"] = strFtpUserName;
                            dr1["strUpdatedTime"] = strUpdatedTime;
                            dtSearch.Rows.Add(dr1);
                            //dr = dt.NewRow();

                            //Store the DataTable in ViewState
                            ViewState["CurrentTable"] = dtSearch;
                        }
                 
                }
                AppDB.Logger.Info("Fetching all the cmc configuration  from DB to the GridView");
                gdviewcmc.DataSource = dtSearch;
                gdviewcmc.DataBind();
            }
       
        }
        else 
        {
            GetAppData();
        }
    }
}