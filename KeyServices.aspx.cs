﻿using LOTServerDBSecurity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using myfashionsServerSDK;

public partial class KeyServices : System.Web.UI.Page
{
    object lockTarget = new object();
    string ip = (string)System.Configuration.ConfigurationSettings.AppSettings["ip"];
    int port = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["port"]);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["username"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                lock (lockTarget)
                {
                    AppDB.Logger.Info("Initiating Create Key Service.");
                    //AppDB.Logger.Info("Checking for Application Name exists in DB... App Name: " + ddlappname.SelectedItem.Text);
                    AppDB.Logger.Info("Checking for Atleast One Application Name exists in DB..");
                    if (AppDB.securityDB.RegApps.ToList().Count > 0)
                    {
                        AppDB.Logger.Info("Fetching all the applications and bind to the dropdownlist");
                        ddlappname.DataSource = AppDB.securityDB.RegApps.ToList();
                        ddlappname.DataBind();
                    }
                }
            }
        }
    }
    protected void ddlstores_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlstores.SelectedItem.Text != "Select")
        {
            lock (lockTarget)
            {
                AppDB.Logger.Info("Fetching the StoreAdmin according to the selected..ddlstores " + ddlstores.SelectedItem.Text + "");
                //LOTServerDBSecurity.Models.SecurityKey secKey = AppDB.securityDB.SecurityKey.Where(c => c.StoreAdmin.UserName.Equals(ddlstores.SelectedItem.Text.Trim(), StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                LOTServerDBSecurity.Models.StoreAdmin StoreAdmin = AppDB.securityDB.StoreAdmin.Where(c => c.UserName.Equals(ddlstores.SelectedItem.Text.Trim(), StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                if (StoreAdmin != null)
                {
                    AppDB.Logger.Info("Fetching all the GUID's according to the selected store..ddlstores" + ddlstores.SelectedItem.Text + "");
                    ddlguid.DataTextField = "GUID";
                    ddlguid.DataValueField = "SNo";
                    ddlguid.DataSource = StoreAdmin.LstRegKeys.ToList();
                    ddlguid.DataBind();
                }
            }
        }
        else
        {
            ddlguid.Items.Clear();
            ddlguid.Items.Add("Select");
        }
      
    }
    protected void ddlappname_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlappname.SelectedItem.Text != "Select")
        {
            lock (lockTarget)
            {
                AppDB.Logger.Info("Fetching the Registered application according to the selected..ddlappname " + ddlappname.SelectedItem.Text + "");
                RegisteredApplications selectedRegApp = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(ddlappname.SelectedItem.Text.Trim(), StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                if (selectedRegApp != null)
                {
                    AppDB.Logger.Info("Fetching all the stores according to the selected..ddlappname " + ddlappname.SelectedItem.Text + "");
                    ddlstores.DataSource = selectedRegApp.LstStoreAdmins.ToList();
                    ddlstores.DataBind();
                }
            }
        }
        else
        {
            ddlstores.Items.Clear();
            ddlstores.Items.Add("Select");
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        AppDB.Logger.Info("Initiating Submit Button in KeyUpdate");
        lock (lockTarget)
        {
            if (chkupdates.Checked == true)
            {
                lock (lockTarget)
                {
                    if (ddlguid.SelectedItem.Text != "Select")
                    {
                        AppDB.Logger.Info("Fetching the security key according to the dropdown selected..ddlguid" + ddlguid.SelectedItem.Text + "");
                        LOTServerDBSecurity.Models.SecurityKey secKey = AppDB.securityDB.SecurityKey.Where(c => c.GUID.Equals(ddlguid.SelectedItem.Text.Trim(), StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                        if (secKey != null)
                        {
                            secKey.IsEnabled = true;
                            AppDB.securityDB.SaveChanges();
                            myfashionsServerSDK.RefreshAPI.RefreshWEBAPI refreshapi = new myfashionsServerSDK.RefreshAPI.RefreshWEBAPI(ip, port);
                            refreshapi.RefreshDB();
                            AppDB.Logger.Info("Key Service Done Successfully..");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Key Service Updated Succesffully'); window.location='" + Request.ApplicationPath + "KeyServices.aspx';", true);
                        }
                    }
                    else
                    {
                        AppDB.Logger.Info("Required fields should not be empty.");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Required fields should not be empty.');", true);
                    }
                }
            }
            else
            {
                lock (lockTarget)
                {
                    if (ddlguid.SelectedItem.Text != "Select")
                    {
                        AppDB.Logger.Info("Fetching the security key according to the dropdown selected..ddlguid" + ddlguid.SelectedItem.Text + "");
                        LOTServerDBSecurity.Models.SecurityKey secKey = AppDB.securityDB.SecurityKey.Where(c => c.GUID.Equals(ddlguid.SelectedItem.Text.Trim(), StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                        if (secKey != null)
                        {
                            secKey.IsEnabled = true;
                            AppDB.securityDB.SaveChanges();
                            myfashionsServerSDK.RefreshAPI.RefreshWEBAPI refreshapi = new myfashionsServerSDK.RefreshAPI.RefreshWEBAPI(ip, port);
                            refreshapi.RefreshDB();
                            AppDB.Logger.Info("Key Service Updated Successfully..");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Key Service Updated Succesffully'); window.location='" + Request.ApplicationPath + "KeyServices.aspx';", true);
                        }
                    }
                    else
                    {
                        AppDB.Logger.Info("Required fields should not be empty.");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Required fields should not be empty.');", true);
                    }
                }
            }
        }
        
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Home.aspx");
    }
}