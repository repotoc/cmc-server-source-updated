﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="ViewConfidentialKeys.aspx.cs" Inherits="ViewConfidentialKeys" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <script type="text/javascript">
	   function Validate() {
		   var gv = document.getElementById("<%=gdviewcnfkey.ClientID%>");
			var rbs = gv.getElementsByTagName("input");
			var flag = 0;
			for (var i = 0; i < rbs.length; i++) {

				if (rbs[i].type == "radio") {
					if (rbs[i].checked) {
						flag = 1;
						break;
					}
				}
			}
			if (flag == 0) {
				alert("Please Select Any One");
				return false;
			}
			else {
				var x = confirm("Are you sure you want to delete?");
				if (x == true)
					return true;
				else {
					if (document.getElementById("<%=Label1.ClientID%>") != null)
						 document.getElementById("<%=Label1.ClientID%>").innerText = "";
					 return false;
				 }
			 }
		 }
</script>
	  <script  type="text/javascript">
		  function SelectSingleRadiobutton(rdbtnid) {
			  var rdBtn = document.getElementById(rdbtnid);
			  var rdBtnList = document.getElementsByTagName("input");
			  for (i = 0; i < rdBtnList.length; i++) {
				  if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id) {
					  rdBtnList[i].checked = false;
				  }
			  }
		  }
</script>
    <script>
        function ValidateFieldLegth(sender, args) {
            var v = document.getElementById('<%=txtsecreykey.ClientID%>').value;
        if (v.length < 6 && v.length > 10) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
        }
    }
</script>
	   <style type="text/css">
.tableBackground
{
	background-color:silver;
	opacity:0.7;
}
</style>
	 <div class="MainContainer">
		 <asp:Button ID="btnNew" runat="server" Text="New Confidential Key" CssClass="btn" OnClick="btnNew_Click" />
		 <asp:Label ID="Label1" runat="server"></asp:Label>
            <div>
            <asp:DropDownList ID="ddlappname" AppendDataBoundItems="true" DataTextField="strAppName" DataValueField="strSno"  AutoPostBack="true" OnSelectedIndexChanged="ddlappname_SelectedIndexChanged" runat="server">
                <asp:ListItem Value="-1">Select</asp:ListItem>
            </asp:DropDownList>
        </div>
		   <div class="grdDiv">
			   <asp:GridView ID="gdviewcnfkey" runat="server"  AutoGenerateColumns="false"  BackColor="#DEBA84"
			BorderColor="#DEBA84" BorderStyle="None"  OnRowDeleting="gdviewcnfkey_RowDeleting" BorderWidth="1px" AllowSorting="true"  OnSorting="gdviewcnfkey_Sorting" CellPadding="3" CellSpacing="2" >
				   <Columns>
					   <asp:TemplateField >
					<ItemTemplate>

						 <asp:RadioButton ID="RadioButton1" runat="server" OnCheckedChanged="RadioButton1_CheckedChanged"  AutoPostBack="true" OnClick="javascript:SelectSingleRadiobutton(this.id)"/>
					</ItemTemplate>
				</asp:TemplateField>
					   <asp:BoundField DataField="strSno" HeaderText="SNo" Visible="false"/>
					   <asp:BoundField DataField="strAppName" HeaderText="Application Name" SortExpression="strAppName" />
					   <asp:BoundField DataField="strkey"  HeaderText="Confidential Key" SortExpression="strkey" />
					   <asp:BoundField  DataField="keyid" HeaderText="KeyID" SortExpression="keyid"/>
				   </Columns>
					  <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
			<HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
			<PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
			<RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
			<SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
			<SortedAscendingCellStyle BackColor="#FFF1D4" />
			<SortedAscendingHeaderStyle BackColor="#B95C30" />
			<SortedDescendingCellStyle BackColor="#F1E5CE" />
			<SortedDescendingHeaderStyle BackColor="#93451F" />
				</asp:GridView>
			   </div>
		 <div id="EditPanel" class="PopUp" visible="false" runat="server">
			<table style="width:50%; height:50%; background:#FFF;
			margin: 0 auto;
			margin-top: 9%;">
				<tr>
					<th>Edit Confidential Text
					</th>
					 <th>
						<asp:ImageButton ID="imgClose" runat="server" ImageUrl="~/images/close.png" OnClick="imgClose_Click" Height="30px" Width="30px"/>
					</th>
				</tr>
				<tr>
					<td style="width: 45%">SNo:
					</td>
					<td>
						<asp:Label ID="lblid" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td>Application Name:
					</td>
					<td>
						<asp:TextBox ID="txtappname" Enabled="false" runat="server" />
					</td>
				</tr>
				<tr>
					<td>Key ID:
					</td>
					<td>
						<asp:TextBox ID="txtkeyid" Enabled="false" runat="server" />
					</td>
				</tr>
				<tr>
					<td>Confidential Text:
					</td>
					<td>
						<asp:TextBox ID="txtsecreykey" runat="server" />
                      
					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align:center;">
						<asp:Button ID="btnUpdate" CommandName="Update" runat="server" Text="Update" OnClick="btnUpdate_Click" CausesValidation="false" />
				   
						<asp:Button ID="btnCancel" runat="server" Text="Reset"  CausesValidation="false" OnClick="btnCancel_Click" />
					</td>
				</tr>
			</table>
		</div>
		 <div>
			 <asp:Button ID="btnEdit" runat="server" Text="Edit" OnClientClick="return RadioCheck();" OnClick="btnEdit_Click" />
			 <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClientClick="return Validate();" OnClick="btnDelete_Click" />
		 </div>
		 </div>

</asp:Content>

