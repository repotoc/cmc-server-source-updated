﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="ViewUsers.aspx.cs" Inherits="ViewUsers" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<style type="text/css">
.tableBackground
{
	background-color:silver;
	opacity:0.7;
}
</style>
	 <script type="text/javascript">
		 function Validate() {
			 var gv = document.getElementById("<%=gdviewusers.ClientID%>");
			 var rbs = gv.getElementsByTagName("input");
			 var flag = 0;
			 for (var i = 0; i < rbs.length; i++) {

				 if (rbs[i].type == "radio") {
					 if (rbs[i].checked) {
						 flag = 1;
						 break;
					 }
				 }
			 }
			 if (flag == 0) {
				 alert("Please Select Any One");
				 return false;
			 }
			 else {
				 var x = confirm("Are you sure you want to delete?");
				 if (x == true)
					 return true;
				 else {
					 if (document.getElementById("<%=Label1.ClientID%>") != null)
						document.getElementById("<%=Label1.ClientID%>").innerText = "";
					return false;
				}
			}
		}
</script>
	  <script  type="text/javascript">
		  function SelectSingleRadiobutton(rdbtnid) {
			  var rdBtn = document.getElementById(rdbtnid);
			  var rdBtnList = document.getElementsByTagName("input");
			  for (i = 0; i < rdBtnList.length; i++) {
				  if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id) {
					  rdBtnList[i].checked = false;
				  }
			  }
		  }
</script>
	
	 <div class="MainContainer">
		 <asp:Button ID="btnnewuser" runat="server" Text="New User" CssClass="btn"  OnClick="btnnewuser_Click"/>
		 <asp:Label ID="Label1" runat="server"></asp:Label>
         <div>
             <asp:DropDownList ID="ddlappname" AppendDataBoundItems="true" DataTextField="AppName" DataValueField="SNo" AutoPostBack="true" OnSelectedIndexChanged="ddlappname_SelectedIndexChanged" runat="server">
                <asp:ListItem Value="-1">Select</asp:ListItem>
            </asp:DropDownList>
             
        </div>
        <div>
            <asp:DropDownList ID="ddlcmc" AppendDataBoundItems="true"  AutoPostBack="true" OnSelectedIndexChanged="ddlcmc_SelectedIndexChanged" runat="server">
                <asp:ListItem Value="-1">Select</asp:ListItem>
            </asp:DropDownList>
        </div>
          <div>
            <asp:DropDownList ID="ddlstorename" AppendDataBoundItems="true"  AutoPostBack="true" OnSelectedIndexChanged="ddlstorename_SelectedIndexChanged" runat="server">
                <asp:ListItem Value="-1">Select</asp:ListItem>
            </asp:DropDownList>
        </div>
		   <div class="grdDiv">
			   <asp:GridView ID="gdviewusers" runat="server"  AutoGenerateColumns="False"  OnRowDeleting="gdviewusers_RowDeleting" AllowSorting="true" OnSorting="gdviewusers_Sorting"  BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" > 
					<Columns>
						 <asp:TemplateField >
						<ItemTemplate>
						 <asp:RadioButton ID="RadioButton1" runat="server"  OnCheckedChanged="RadioButton1_CheckedChanged" OnClick="javascript:SelectSingleRadiobutton(this.id)" />
					  </ItemTemplate>
						 </asp:TemplateField>
						<asp:BoundField DataField="strSNo" HeaderText="Sno" Visible="false" />
						<asp:BoundField DataField="strAppName" HeaderText="Application Name" SortExpression="strAppName" />
						<asp:BoundField DataField="strConfig" HeaderText="Configuration Name" SortExpression="strConfig" />
						<asp:BoundField DataField="strStoreName" HeaderText="Store Name" SortExpression="strStoreName" />
						<asp:BoundField DataField="strStoreEmail" HeaderText="Store Email"  SortExpression="strStoreEmail" />
						<asp:BoundField DataField="strStoreContactNumber" HeaderText="Store Contact Number" SortExpression="strStoreContactNumber" />
						<asp:BoundField DataField="strUserName" HeaderText="User Name" SortExpression="strUserName" />
						<asp:BoundField DataField="strUpdatedTime" HeaderText="Updated Time" SortExpression="strUpdatedTime" />
						<asp:BoundField DataField="strIsUpdate" HeaderText="Update Status" SortExpression="strIsUpdate" />
						<asp:BoundField  DataField="strIsActive" HeaderText="Service Status" SortExpression="strIsActive"/>
					</Columns>
					<FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
					<HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
					<PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
					<RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
					<SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
					<SortedAscendingCellStyle BackColor="#FFF1D4" />
					<SortedAscendingHeaderStyle BackColor="#B95C30" />
					<SortedDescendingCellStyle BackColor="#F1E5CE" />
					<SortedDescendingHeaderStyle BackColor="#93451F" />
				</asp:GridView>
			   </div>
		  <div id="EditPanel" class="PopUp" visible="false" runat="server">
			<table style="width:50%; height:50%; background:#FFF;
			margin: 0 auto;
			margin-top: 9%;">
				<tr>
					<th colspan="2">Edit User Details
					</th>
					 <th>
						<asp:ImageButton ID="imgClose" runat="server" ImageUrl="~/images/close.png" OnClick="imgClose_Click" Height="30px" Width="30px"/>
					</th>
				</tr>
				
				<tr>
					<td>Selected Application</td>
					<td><asp:TextBox ID="txtApplicationName" runat="server" Enabled="false"></asp:TextBox></td>
				</tr>
				 <tr>
					<td>Selected configuration</td>
					<td><asp:TextBox ID="txtConfigName" runat="server" Enabled="false" ></asp:TextBox></td>
				</tr>
				<tr>
							<td align="right" style="width: 45%">SNo:
							</td>
							<td>
								<asp:Label ID="lblid" runat="server"></asp:Label>
							</td>
						</tr>
				<tr>
							<td align="right">Store Name:
							</td>
							<td>
								<asp:TextBox ID="txtStoreName" pattern="^[A-Za-z0-9]+$" ToolTip="Enter only AlphaNumeric characters"  runat="server" Enabled="false" />
							</td>
						</tr>
				<tr>
							<td align="right">Store Email:
							</td>
							<td>
								<asp:TextBox ID="txtStoreEmail"  pattern="^[a-z][a-z|0-9|]*([_][a-z|0-9]+)*([.][a-z|0-9]+([_][a-z|0-9]+)*)?@[a-z][a-z|0-9|]*\.([a-z][a-z|0-9]*(\.[a-z][a-z|0-9]*)?)$" ToolTip="Enter Valid E-Mail"  runat="server" />
							</td>
					<tr>
							<td align="right">Store Contact Number:
							</td>
							<td>
								<asp:TextBox ID="txtStoreContactNumber"  required="required" ToolTip="Enter only 10 digit Mobile Number" pattern="[789][0-9]{9}"   runat="server" />
							</td>
						</tr>
						</tr>
						<tr>
							<td align="right">UserID:
							</td>
							<td>
								<asp:TextBox ID="txtuserid" Enabled="false" runat="server" />
							</td>
						</tr>
						<tr>
							<td align="right">User Name:
							</td>
							<td>
								<asp:TextBox ID="txtusername" runat="server" />
							</td>
						</tr>
						<tr>
							<td align="right">Password:
							</td>
							<td>
								<asp:TextBox ID="txtpwd" runat="server" />
							</td>
						</tr>
				 <tr>
					<td colspan="2">
				<asp:Label ID="lblupdateavail1" runat="server" Text="Update Status" Visible="false"></asp:Label>
				<asp:RadioButtonList ID="radList" runat="server" Width="300px"  RepeatColumns="2"  Visible="false" RepeatDirection="Horizontal" Height="20px" >
					<asp:ListItem>Enable</asp:ListItem>
					<asp:ListItem>Disable</asp:ListItem>
				</asp:RadioButtonList>
			   

			</td>
				</tr>
						 <tr>
					<td colspan="2">
				<asp:Label ID="lblstatus" runat="server" Text="User Status" Visible="false"></asp:Label>
				<asp:RadioButtonList ID="raduserstatus" runat="server" Width="300px"  RepeatColumns="2" Visible="false" RepeatDirection="Horizontal" Height="20px" >
					<asp:ListItem>Active</asp:ListItem>
					<asp:ListItem>Inactive</asp:ListItem>
				
				</asp:RadioButtonList>
			   

			</td>
				</tr>
						<tr>
							<td align="right">
								<asp:Button ID="btnUpdate" CommandName="Update" runat="server" Text="Update" OnClick="btnUpdate_Click" />
							</td>
							<td>
								<asp:Button ID="btnCancel" runat="server" Text="Reset"  CausesValidation="false" OnClick="btnCancel_Click"/>
							</td>
						</tr>
				</table>
			  </div>
		 <div>
				<asp:Button ID="btnEdit" runat="server" Text="Edit"  OnClientClick="return RadioCheck();" OnClick="btnEdit_Click" />
			<asp:Button ID="btnDelete" runat="server" Text="Delete"  OnClientClick="return Validate();" OnClick="btnDelete_Click" />
		 </div>
		 </div>
</asp:Content>

