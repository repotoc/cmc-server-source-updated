﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="UserRegistration.aspx.cs" Inherits="UserRegistration" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
    
    <style>
         .table > tbody > tr > td {border:none;
            }
    </style>
    <table class="table"  style="width:50%;margin:62px 22px 0px 280px;">
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Select Application:</td>
            <td>
                 <asp:DropDownList ID="ddlApp" runat="server" AppendDataBoundItems="true" CssClass="textboxstyle" AutoPostBack="true"  DataTextField="AppName" DataValueField="SNo" OnSelectedIndexChanged="ddlApp_SelectedIndexChanged">
                    <asp:ListItem Value="-1">Select</asp:ListItem>
                </asp:DropDownList>
                <br/>
            </td>
        </tr>
        <tr>
            <td>Select Configuration Name:</td>
            <td>
                 <asp:DropDownList ID="ddlConfig" runat="server" AppendDataBoundItems="true" CssClass="textboxstyle" AutoPostBack="true"  DataTextField="ConfigurationName" DataValueField="SNo">
                    <asp:ListItem Value="-1">Select</asp:ListItem>
                </asp:DropDownList>
                <br/>
            </td>
        </tr>
       
         <tr>
            <td>Store Name:</td>
            <td>
                <asp:TextBox ID="txtStoreName" runat="server" pattern="^[A-Za-z0-9]+$" ToolTip="Enter only AlphaNumeric characters" CssClass="textboxstyle" required="required" ></asp:TextBox>
            </td>
            
        </tr>
         <tr>
            <td>Store Email:</td>
            <td>
                <asp:TextBox ID="txtStoreEmail" runat="server" required="required" pattern="^[a-z][a-z|0-9|]*([_][a-z|0-9]+)*([.][a-z|0-9]+([_][a-z|0-9]+)*)?@[a-z][a-z|0-9|]*\.([a-z][a-z|0-9]*(\.[a-z][a-z|0-9]*)?)$" ToolTip="Enter Valid E-Mail" CssClass="textboxstyle"></asp:TextBox>
                <br/>
            </td>
        </tr>
         <tr>
            <td>Store Contact Number:</td>
            <td>
                 <asp:TextBox ID="txtStoreContactNumber" runat="server" required="required" ToolTip="Enter only 10 digit mobile number" pattern="[789][0-9]{9}"  CssClass="textboxstyle"></asp:TextBox>                
            </td>
             
        </tr>
         <tr>
            <td>UserID :</td>
            <td><asp:TextBox ID="txtuserid" runat="server" Enabled="false" CssClass="textboxstyle" ></asp:TextBox>
                <br />
            </td>
        </tr>
        <tr>
            <td>User Name:</td>
            <td><asp:TextBox ID="txtUserName" runat="server" CssClass="textboxstyle" required="required" ></asp:TextBox>
                 <asp:RegularExpressionValidator ID="rev1" runat="server" ControlToValidate="txtUserName" ErrorMessage="Please Enter Valid UserName" ForeColor="Red" Font-Size="Smaller" ValidationExpression="^[a-zA-Z0-9_@.-]{3,20}$"  Display="Dynamic" ></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "txtUserName" ID="RegularExpressionValidator3" ForeColor="Red" Font-Size="Smaller" ValidationExpression = "^[\s\S]{3,}$" runat="server" ErrorMessage="Minimum 3  characters required."></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "txtUserName" ID="RegularExpressionValidator4" ForeColor="Red" Font-Size="Smaller" ValidationExpression ="^[\s\S]{0,20}$" runat="server" ErrorMessage="Limit Should Not Exceed 20 characters."></asp:RegularExpressionValidator>
                <br/>
            </td>
        </tr>
          <tr>
            <td>Password:</td>
            <td><asp:TextBox ID="txtPassword" runat="server" TextMode="Password"  CssClass="textboxstyle" required="required"></asp:TextBox>
                 <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" display="Dynamic"  ControlToValidate="txtPassword"  ErrorMessage="Password must contain one of @#$%^&*/." ForeColor="Red" Font-Size="Smaller"  ValidationExpression=".*[@#$%^&*/].*" />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" display="Dynamic"  ControlToValidate="txtPassword"   ErrorMessage="Password must be 4-16 nonblank characters."  ForeColor="Red" Font-Size="Smaller" ValidationExpression="[^\s]{4,16}" />
                <br/>
            </td>
        </tr>
       
         <tr>
              <td colspan="2">
                <asp:Label ID="lblenabled" runat="server" Text="User Status" Visible="false"></asp:Label>
                <asp:RadioButtonList ID="radList" runat="server" Width="300px" Visible="false"  RepeatColumns="2" RepeatDirection="Horizontal" Height="20px" >
                    <asp:ListItem>Enable</asp:ListItem>
                    <asp:ListItem>Disable</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
          <tr>
             <td colspan="2">
                <asp:Label ID="lblupdateavail" runat="server" Text="Update Status" Visible="false"></asp:Label>
                <asp:RadioButtonList ID="radupdate" runat="server" Width="300px"  Visible="false" RepeatColumns="2" RepeatDirection="Horizontal" Height="20px" >
                    <asp:ListItem>Active</asp:ListItem>
                    <asp:ListItem>Inactive</asp:ListItem>
                </asp:RadioButtonList>
               

            </td>
        </tr>
          <tr>
            <td colspan="2" style="text-align:center">
                <asp:Button ID="btnSubmit" runat="server"  Text="Submit"   CssClass="btn"  OnClick="btnSubmit_Click"/>
                <asp:Button ID="btnCancel" runat="server"  Text="Cancel"   CssClass="btn"  OnClick="btnCancel_Click"/>
            </td>
         
            
        </tr>
        </table>
    
</asp:Content>

