﻿using LOTServerDBSecurity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;



public partial class RegisterApp : System.Web.UI.Page
{
    object lockTarget = new object();
    string ip = (string)System.Configuration.ConfigurationSettings.AppSettings["ip"];
    int port = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["port"]);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["username"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {

            }
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        lock (lockTarget)
        {
            AppDB.Logger.Info("Initiating Post Application: " + txtAppName.Text.Trim() + " Started.");
            if (!string.IsNullOrEmpty(txtAppName.Text) && !string.IsNullOrEmpty(txtversion.Text))
            {
                if (!string.IsNullOrEmpty(txtAppName.Text))
                {
                    if (!string.IsNullOrEmpty(txtversion.Text))
                    {
                        if (!AppDB.securityDB.RegApps.ToList().Exists(c => c.AppName.Equals(txtAppName.Text.Trim(),StringComparison.OrdinalIgnoreCase)))
                        {
                            AppDB.Logger.Info("Application Doesn't exist. Saving Started.");
                            try
                            {
                                lock (lockTarget)
                                {
                                    RegisteredApplications regApp = new RegisteredApplications();
                                    regApp.AppName = txtAppName.Text.Trim();
                                    regApp.Version = txtversion.Text.Trim('.');
                                    regApp.IsUpdateAvailable = true;
                                    regApp.IsActive = true;
                                    regApp.UpdatedTime = DateTime.Now;
                                    AppDB.securityDB.RegApps.Add(regApp);
                                    AppDB.securityDB.SaveChanges();
                                    myfashionsServerSDK.RefreshAPI.RefreshWEBAPI refreshapi = new myfashionsServerSDK.RefreshAPI.RefreshWEBAPI(ip, port);
                                    refreshapi.RefreshDB();
                                }
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Application created Successfully'); window.location='" + Request.ApplicationPath + "ViewApps.aspx';", true);
                            }
                            catch (Exception ex)
                            {
                                AppDB.Logger.ErrorException(ex.Message, ex);
                                Response.Redirect("ViewApps.aspx");
                            }
                        }
                        else
                        {
                            AppDB.Logger.Info("Application Already Exists.");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Application Already Exists.');", true);
                        }
                    }
                    else
                    {
                        AppDB.Logger.Info("Version should not be empty.");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Version should not be empty.');", true);
                    }
                }
                else
                {
                    AppDB.Logger.Info("Application Name should not be empty.");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Application Name should not be empty.');", true);
                }
            }
            else
            {
                 
                AppDB.Logger.Info("Required Fields should Not Be Empty.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Required Fields should Not Be Empty.');", true);
            }        }
    }

    private void clearAll()
    {
        txtAppName.Text = "";
        txtversion.Text = "";
    }
   
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtAppName.Text = "";
        txtversion.Text = "";
        Response.Redirect("ViewApps.aspx");
    }

}