﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="ViewApps.aspx.cs" Inherits="ViewApps" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style type="text/css">
     
    </style>

    <style type="text/css">

      

        .modalBackground {
            background-color: Gray;
            filter: alpha(opacity=80);
            opacity: 0.8;
            z-index: 10000;
        }

        .tableBackground {
            background-color: silver;
            opacity: 0.7;
        }
    </style>
    <script type="text/javascript">
        function isDelete() {
            return confirm("Do you want to delete this details ?");
        }
    </script>
    <script type="text/javascript">
        function Validate() {
            var gv = document.getElementById("<%=gdviewapp.ClientID%>");
             var rbs = gv.getElementsByTagName("input");
             var flag = 0;
             for (var i = 0; i < rbs.length; i++) {

                 if (rbs[i].type == "radio") {
                     if (rbs[i].checked) {
                         flag = 1;
                         break;
                     }
                 }
             }
             if (flag == 0) {
                 alert("Please Select Any One");
                 return false;
             }
             else {
                 var x = confirm("Are you sure you want to delete?");
                 if (x == true)
                     return true;
                 else {
                     if (document.getElementById("<%=Label1.ClientID%>") != null)
                         document.getElementById("<%=Label1.ClientID%>").innerText = "";
                        return false;
                    }
                }
            }
</script>
    
    <script  type="text/javascript">
        function SelectSingleRadiobutton(rdbtnid) {
            var rdBtn = document.getElementById(rdbtnid);
            var rdBtnList = document.getElementsByTagName("input");
            for (i = 0; i < rdBtnList.length; i++) {
                if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id) {
                    rdBtnList[i].checked = false;
                }
            }
        }
</script>
  
    <div class="MainContainer">
         <asp:Button ID="btnNew" runat="server" Text="New Application" CssClass="btn" CausesValidation="false" OnClick="btnNew_Click" />
       <asp:Label ID="Label1" runat="server"></asp:Label>
        <div>
            <asp:DropDownList ID="ddlappname" AppendDataBoundItems="true" DataTextField="strAppName" DataValueField="strSno" AutoPostBack="true" OnSelectedIndexChanged="ddlappname_SelectedIndexChanged" runat="server">
                <asp:ListItem Value="-1">Select</asp:ListItem>
            </asp:DropDownList>
        </div>
        
        <div class="grdDiv">
        <asp:GridView ID="gdviewapp" runat="server" AutoGenerateColumns="False"  BackColor="#DEBA84" AllowSorting="true" OnSorting="gdviewapp_Sorting"
            BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" 
            OnRowDeleting="gdviewapp_RowDeleting">
            <Columns>
                <asp:TemplateField >
                    <ItemTemplate>
                         <asp:RadioButton ID="RadioButton1" runat="server" OnCheckedChanged="RadioButton1_CheckedChanged" OnClick="javascript:SelectSingleRadiobutton(this.id)" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="strAppName" HeaderText="Application Name"  SortExpression="strAppName"/>
                <asp:BoundField DataField="strVerison" HeaderText="Version" SortExpression="strVerison" />
                <asp:BoundField DataField="strUpdatedTime" HeaderText="Updated Time" SortExpression="strUpdatedTime" />
                <asp:BoundField DataField="strUpdateAvail" HeaderText="Update Status" SortExpression="strUpdateAvail"/>
                <asp:BoundField DataField="strIsActive" HeaderText="Service Status" SortExpression="strIsActive"/>
            </Columns>
            <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
            <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
            <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
            <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#FFF1D4" />
            <SortedAscendingHeaderStyle BackColor="#B95C30" />
            <SortedDescendingCellStyle BackColor="#F1E5CE" />
            <SortedDescendingHeaderStyle BackColor="#93451F" />
        </asp:GridView>
            </div>
        <div id="EditPanel" class="PopUp" visible="false" runat="server">
            <table style="width:50%; height:50%; background:#FFF;
            margin: 0 auto;
            margin-top: 9%;">
                <tr>
                    <th>Edit App Details
                    </th>
                    <th>
                        <asp:ImageButton ID="imgClose" runat="server" ImageUrl="~/images/close.png"  Height="30px" Width="30px" OnClick="imgClose_Click"/>
                    </th>
                </tr>
                <tr>
                    <td>App Name:
                    </td>
                    <td>
                        <asp:TextBox ID="txtappname" runat="server"   Enabled="false"/>
                    </td>
                </tr>
                <tr>
                    <td>Last Updated Date:
                    </td>
                    <td>
                        <asp:TextBox ID="txtupdatedtime" runat="server"  Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td>Version Number:
                    </td>
                    <td>
                        <asp:TextBox ID="txtversion" runat="server"/>
                        <asp:RegularExpressionValidator ID="rev1" runat="server" ControlToValidate="txtversion" ForeColor="Red" Font-Size="Smaller" ErrorMessage="Version Should Be Only Alphanumerics only" ValidationExpression="^[0-9a-zA-Z.]+$" ></asp:RegularExpressionValidator>
                 <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "txtversion" ID="RegularExpressionValidator5" ForeColor="Red" Font-Size="Smaller" ValidationExpression = "^[\s\S]{1,}$" runat="server" ErrorMessage="Minimum 1  characters required."></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "txtversion" ID="RegularExpressionValidator6" ForeColor="Red" Font-Size="Smaller" ValidationExpression ="^[\s\S]{0,8}$" runat="server" ErrorMessage="Limit Should Not Exceed 8 characters."></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                <asp:Label ID="lblupdateavail" runat="server" Text="Update Status" Visible="false"></asp:Label>
                <asp:RadioButtonList ID="radList" runat="server" Width="300px"  RepeatColumns="2" Visible="false"  AutoPostBack="true" RepeatDirection="Horizontal" Height="20px" >
                    <asp:ListItem>Active</asp:ListItem>
                    <asp:ListItem>Inactive</asp:ListItem>
                </asp:RadioButtonList>
            </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:center;">
                        <asp:Button ID="btnUpdate" CommandName="Update" runat="server"  Text="Update" OnClick="btnUpdate_Click" />
                   
                        <asp:Button ID="btnCancel" runat="server" Text="Reset"  CausesValidation="false"  OnClick="btnCancel_Click" />
                    </td>
                </tr>
            </table>
        </div>
  <div>
      <asp:Button ID="btnEdit" runat="server" Text="Edit"  OnClientClick="return RadioCheck();" OnClick="btnEdit_Click" />
      <asp:Button ID="btnDelete" runat="server" Text="Delete"  OnClientClick="return Validate();"  OnClick="btnDelete_Click"/>
  </div>
    </div>
</asp:Content>

