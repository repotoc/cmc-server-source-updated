﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MyFashions</title>
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/jquery.bxslider.css" rel="stylesheet" />
    <link href="css/modern-business.css" rel="stylesheet" />
    <link href="css/style3.css" rel="stylesheet" />
    <style>
        .table {
    width: 45%;
    margin-bottom: 20px;
    margin: 0 25%;
}
            .table > tbody > tr > td {border:none;
            }
    </style>
  
</head>
<body>
    <form id="form1" runat="server">
    <div style="margin:62px 22px 0px 152px;width:80%;">
    <table  class="table" >
        <tr>
            <td  style="width:50%;text-align: center;">User Name:</td>
            <td  style="width:50%;"><asp:TextBox ID="txtusername" runat="server" CssClass="textboxstyle"></asp:TextBox></td>
        </tr>
        <tr>
            <td  style="width:50%;text-align: center;">Password:</td>
            <td><asp:TextBox ID="txtpwd" runat="server" TextMode="Password"  oncopy="return false" onpaste="return false"
            oncut="return false" CssClass="textboxstyle"></asp:TextBox></td>
        </tr>
        <tr>
            <td  style="text-align:right;"><asp:Button ID="btnSubmit" runat="server"  OnClick="btnSubmit_Click" CssClass="btn" Text="Submit" /></td>
            <td><asp:Button ID="btnCancel" runat="server"  Text="Cancel" OnClick="btnCancel_Click" CssClass="btn" /></td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
