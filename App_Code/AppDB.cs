﻿using LOTServerDBSecurity.DBFolder;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AppDB
/// </summary>
public static class AppDB
{
    public static LotServerDBSecurityContext securityDB = new LotServerDBSecurityContext();
	public static Logger Logger = LogManager.GetLogger("myfashionAPI");
}