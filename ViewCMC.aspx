﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="ViewCMC.aspx.cs" Inherits="ViewCMC" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<style type="text/css">
.tableBackground
{
	background-color:silver;
	opacity:0.7;
}
</style>
	 <script type="text/javascript">
		 function Validate() {
			 var gv = document.getElementById("<%=gdviewcmc.ClientID%>");
			var rbs = gv.getElementsByTagName("input");
			var flag = 0;
			for (var i = 0; i < rbs.length; i++) {

				if (rbs[i].type == "radio") {
					if (rbs[i].checked) {
						flag = 1;
						break;
					}
				}
			}
			if (flag == 0) {
				alert("Please Select Any One");
				return false;
			}
			else {
				var x = confirm("Are you sure you want to delete?");
				if (x == true)
					return true;
				else {
					if (document.getElementById("<%=Label1.ClientID%>") != null)
						 document.getElementById("<%=Label1.ClientID%>").innerText = "";
					 return false;
				 }
			 }
		 }
</script>
	  <script  type="text/javascript">
		  function SelectSingleRadiobutton(rdbtnid) {
			  var rdBtn = document.getElementById(rdbtnid);
			  var rdBtnList = document.getElementsByTagName("input");
			  for (i = 0; i < rdBtnList.length; i++) {
				  if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id) {
					  rdBtnList[i].checked = false;
				  }
			  }
		  }
</script>
	<div class="MainContainer">
		<asp:Button ID="btnNew" runat="server" Text="New CMC Configuration" CssClass="btn" OnClick="btnNew_Click" />
		<asp:Label ID="Label1" runat="server"></asp:Label>
        <div>
             <asp:DropDownList ID="ddlappname" AppendDataBoundItems="true" DataTextField="AppName" DataValueField="SNo" AutoPostBack="true" OnSelectedIndexChanged="ddlappname_SelectedIndexChanged" runat="server">
                <asp:ListItem Value="-1">Select</asp:ListItem>
            </asp:DropDownList>
             
        </div>
        <div>
            <asp:DropDownList ID="ddlcmc" AppendDataBoundItems="true"  AutoPostBack="true" OnSelectedIndexChanged="ddlcmc_SelectedIndexChanged" runat="server">
                <asp:ListItem Value="-1">Select</asp:ListItem>
            </asp:DropDownList>
        </div>
		<div class="grdDiv">
			<asp:GridView ID="gdviewcmc" runat="server" AutoGenerateColumns="False"   BackColor="#DEBA84" AllowSorting="true" OnSorting="gdviewcmc_Sorting" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2"  OnRowDeleting="gdviewcmc_RowDeleting" >
				   <Columns>
					   <asp:TemplateField >
						<ItemTemplate>
						 <asp:RadioButton ID="RadioButton1" runat="server"  OnCheckedChanged="RadioButton1_CheckedChanged" OnClick="javascript:SelectSingleRadiobutton(this.id)" />
					  </ItemTemplate>
						 </asp:TemplateField>
					   <asp:BoundField DataField="strSno"  HeaderText="SNo" Visible="false"/>
					   <asp:BoundField DataField="strAppName" HeaderText="Application Name" SortExpression="strAppName" />
					   <asp:BoundField DataField="strConfigName" HeaderText="Configuration Name"  SortExpression="strConfigName" />
					   <asp:BoundField DataField="strFtpUserName" HeaderText="FTP User Name" SortExpression="strFtpUserName" />
					   <asp:BoundField DataField="strUpdatedTime" HeaderText="Updated Time" SortExpression="strUpdatedTime" />
				   </Columns>
					 <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
					<HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
					<PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
					<RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
					<SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
					<SortedAscendingCellStyle BackColor="#FFF1D4" />
					<SortedAscendingHeaderStyle BackColor="#B95C30" />
					<SortedDescendingCellStyle BackColor="#F1E5CE" />
					<SortedDescendingHeaderStyle BackColor="#93451F" />
				</asp:GridView>
			 <div id="EditPanel" class="PopUp" visible="false" runat="server">
			<table style="width:50%; height:50%; background:#FFF;
			margin: 0 auto;
			margin-top: 5%;">
				<tr>
					<th>Edit CMC
					</th>
					 <th>
						<asp:ImageButton ID="imgClose" runat="server" ImageUrl="~/images/close.png" OnClick="imgClose_Click" Height="30px" Width="30px"/>
					</th>
				</tr>
				<tr>
							<td align="right" style="width: 45%">SNo:
							</td>
							<td>
								<asp:Label ID="lblid" runat="server"></asp:Label>
							</td>
						</tr>
						<tr>
							<td align="right">Application Name:
							</td>
							<td>
								<asp:TextBox ID="txtAppName" runat="server" Enabled="false" CssClass="textboxstyle"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td align="right">Configuration Name:
							</td>
							<td>
								<asp:TextBox ID="txtConfigName" Enabled="false"  CssClass="textboxstyle"  runat="server" />
							</td>
						</tr>
				<tr>
							<td align="right">FTP Path:
							</td>
							<td>
								<asp:TextBox ID="txtftppath" CssClass="textboxstyle" required="required" runat="server" />
							</td>
						</tr>
				<tr>
							<td align="right">FTP UserName:
							</td>
							<td>
								<asp:TextBox ID="txtftpusername" required="required" CssClass="textboxstyle"  runat="server" />
							</td>
						</tr>
				<tr>
							<td align="right">FTP Pasword:
							</td>
							<td>
								<asp:TextBox ID="txtftppwd" runat="server" required="required"  CssClass="textboxstyle"  />
							</td>
						</tr>
				<tr>
							<td align="right">Client Location Drive:
							</td>
							<td>
								<asp:TextBox ID="txtclientlocpath" runat="server" required="required" CssClass="textboxstyle"  />
							</td>
						</tr>
				
				<tr>
							<td align="right">Lauch Application Path:
							</td>
							<td>
								<asp:TextBox ID="txtlaunchapppath" runat="server" required="required" CssClass="textboxstyle"  />
							</td>
						</tr>
				 <tr>
					<td></td>
					<td>
						<asp:CheckBox ID="chkActive" runat="server" Text="Is Active" Visible="false" />
					</td>
				</tr>
				<tr>
							<td align="right">
								<asp:Button ID="btnUpdate" CommandName="Update" runat="server" Text="Update" OnClick="btnUpdate_Click" />
							</td>
							<td>
								<asp:Button ID="btnCancel" runat="server" Text="Reset" OnClick="btnCancel_Click" />
							</td>
						</tr>
				</table>
				 </div>
			</div>
		<div>
			<asp:Button ID="btnEdit" runat="server" Text="Edit"  OnClientClick="return RadioCheck();" OnClick="btnEdit_Click" />
			<asp:Button ID="btnDelete" runat="server" Text="Delete" CausesValidation="false"  OnClientClick="return Validate();" OnClick="btnDelete_Click" />
		</div>
	   </div>
	
</asp:Content>

