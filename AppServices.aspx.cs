﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class AppServices : System.Web.UI.Page
{
    object lockTarget = new object();
    string ip = (string)ConfigurationSettings.AppSettings["ip"];
    int port = Convert.ToInt32(ConfigurationSettings.AppSettings["port"]);
    protected void Page_Load(object sender, EventArgs e)
    {
        AppDB.securityDB = new LOTServerDBSecurity.DBFolder.LotServerDBSecurityContext();
        if (Session["username"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                lock (lockTarget)
                {
                    AppDB.Logger.Info("Initiating App Service.");
                    //AppDB.Logger.Info("Checking for Application Name exists in DB... App Name: " + ddlappname.SelectedItem.Text);
                    AppDB.Logger.Info("Checking for Atleast One Application Name exists in DB..");
                    if (AppDB.securityDB.RegApps.ToList().Count > 0)
                    {
                        lock (lockTarget)
                        {
                            AppDB.Logger.Info("Fetching all the applications and bind to the dropdownlist");
                            ddlappname.DataSource = AppDB.securityDB.RegApps.ToList();
                            ddlappname.DataBind();
                        }
                    }
                }
            }
        }
    
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (chkupdates.Checked == true)
        {
            AppDB.Logger.Info("Initiating Submit Button when IsActive");
            lock (lockTarget)
            {
                if (ddlappname.SelectedItem.Text != "Select")
                {
                    AppDB.Logger.Info("Updating App Service: " + ddlappname.SelectedItem.Text.Trim() + " Started.");
                    AppDB.Logger.Info("Fetching the Registered Application according to the selected..ddlappname "+ddlappname.SelectedItem.Text+"");
                    lock (lockTarget)
                    {
                        if (AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(ddlappname.SelectedItem.Text.Trim(), StringComparison.OrdinalIgnoreCase)).ToList().Count > 0)
                        {
                            LOTServerDBSecurity.Models.RegisteredApplications regapp = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(ddlappname.SelectedItem.Text.Trim(), StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                            regapp.IsActive = true;
                        }
                       
                    }
                    lock (lockTarget)
                    {
                        AppDB.Logger.Info("Fetching all the StoreAdmins according to the selected..ddlappname " + ddlappname.SelectedItem.Text + "");
                        List<LOTServerDBSecurity.Models.StoreAdmin> StoreAdmin = AppDB.securityDB.StoreAdmin.Where(c => c.RegApp.AppName.Equals(ddlappname.SelectedItem.Text.Trim(), StringComparison.OrdinalIgnoreCase)).ToList();
                        if (StoreAdmin != null)
                        {
                            foreach (var item in StoreAdmin)
                            {
                                item.IsActive = true;
                                AppDB.Logger.Info("Fetching all the  security keys according to the selected..Store Admins " + item.UserName + "");
                                lock (lockTarget)
                                {
                                    List<LOTServerDBSecurity.Models.SecurityKey> secKey = AppDB.securityDB.SecurityKey.Where(c => c.StoreAdmin.UserName.Equals(item.UserName)).ToList();
                                    foreach (var item1 in secKey)
                                    {
                                        item1.IsEnabled = true;
                                    }
                                }
                            }
                        }
                        AppDB.securityDB.SaveChanges();
                        myfashionsServerSDK.RefreshAPI.RefreshWEBAPI refreshapi = new myfashionsServerSDK.RefreshAPI.RefreshWEBAPI(ip, port);
                        refreshapi.RefreshDB();
                        AppDB.Logger.Info("AppService Updated Successfully");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('AppService Updated Succesffully'); window.location='" + Request.ApplicationPath + "AppServices.aspx';", true);
                    }
                  
                }
                else
                {
                    AppDB.Logger.Info("Required fields should not be empty.");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Required fields should not be empty.');", true);
                }
                }
           
        }
        else
        {
            AppDB.Logger.Info("Initiating Submit Button when Not IsActive");
            lock (lockTarget)
            {
                if (ddlappname.SelectedItem.Text != "Select")
                {
                    AppDB.Logger.Info("Updating App Service: " + ddlappname.SelectedItem.Text.Trim() + " Started.");
                    AppDB.Logger.Info("Fetching the Registered Application according to the selected..ddlappname " + ddlappname.SelectedItem.Text + "");
                    lock (lockTarget)
                    {
                        if (AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(ddlappname.SelectedItem.Text.Trim(), StringComparison.OrdinalIgnoreCase)).ToList().Count > 0)
                        {
                            LOTServerDBSecurity.Models.RegisteredApplications regapp = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(ddlappname.SelectedItem.Text.Trim(), StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                            regapp.IsActive = false;
                        }
                    }
                    lock (lockTarget)
                    {
                        AppDB.Logger.Info("Fetching all the StoreAdmins according to the selected..ddlappname " + ddlappname.SelectedItem.Text + "");
                        List<LOTServerDBSecurity.Models.StoreAdmin> StoreAdmin = AppDB.securityDB.StoreAdmin.Where(c => c.RegApp.AppName.Equals(ddlappname.SelectedItem.Text.Trim(), StringComparison.OrdinalIgnoreCase)).ToList();
                        if (StoreAdmin != null)
                        {
                            foreach (var item in StoreAdmin)
                            {
                                item.IsActive = false;
                                AppDB.Logger.Info("Fetching all the  security keys according to the selected..Store Admins " + item.UserName + "");
                                lock (lockTarget)
                                {
                                    List<LOTServerDBSecurity.Models.SecurityKey> secKey = AppDB.securityDB.SecurityKey.Where(c => c.StoreAdmin.UserName.Equals(item.UserName)).ToList();
                                    foreach (var item1 in secKey)
                                    {
                                        item1.IsEnabled = false;
                                    }
                                }
                            }
                        }
                      
                        AppDB.securityDB.SaveChanges();
                        myfashionsServerSDK.RefreshAPI.RefreshWEBAPI refreshapi = new myfashionsServerSDK.RefreshAPI.RefreshWEBAPI(ip, port);
                        refreshapi.RefreshDB();
                    }
                 
                    AppDB.Logger.Info("AppService Updated Successfully");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('AppService Updated Succesffully'); window.location='" + Request.ApplicationPath + "AppServices.aspx';", true);
                }
                else
                {
                    AppDB.Logger.Info("Required fields should not be empty.");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Required fields should not be empty.');", true);
                }
               
            }
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Home.aspx");
    }
    protected void ddlappname_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlappname.SelectedItem.Text != "Select")
        {
            lock (lockTarget)
            {
                LOTServerDBSecurity.Models.RegisteredApplications regApp = AppDB.securityDB.RegApps.Where(c => c.AppName.Equals(ddlappname.SelectedItem.Text)).FirstOrDefault();
                if (regApp != null)
                {
                    if (regApp.IsActive == true)
                    {
                        chkupdates.Checked = true;
                    }
                    else
                    {
                        chkupdates.Checked = false;
                    }
                }
            }
          
        }
        else
        {
            AppDB.Logger.Info("Required fields should not be empty.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Required fields should not be empty.');", true);
        }
    }
}