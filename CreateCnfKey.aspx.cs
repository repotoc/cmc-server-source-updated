﻿using LOTServerDBSecurity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CreateCnfKey : System.Web.UI.Page
{
    object lockTarget = new object();
    string ip = (string)System.Configuration.ConfigurationSettings.AppSettings["ip"];
    int port = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["port"]);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["username"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                lock (lockTarget)
                {
                    AppDB.Logger.Info("Initiating Create confidential Key.");
                    //AppDB.Logger.Info("Checking for Application Name exists in DB... App Name: " + ddlappname.SelectedItem.Text);
                    AppDB.Logger.Info("Checking for Atleast One Application Name exists in DB..");
                    if (AppDB.securityDB.RegApps.ToList().Count > 0)
                    {
                        lock (lockTarget)
                        {
                            AppDB.Logger.Info("Fetching all the applications and bind to the dropdownlist");
                            ddlappname.DataSource = AppDB.securityDB.RegApps.ToList();
                            ddlappname.DataBind();
                        }
                    }
                }
            }
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtsecretkey.Text) && ddlappname.SelectedItem.Text != "Select")
            {
                AppDB.Logger.Info("Initiating Create Confidential Key.");
                AppDB.Logger.Info("Checking for Application Name exists in DB... App Name: " + ddlappname.SelectedItem.Text);
                if (AppDB.securityDB.RegApps.ToList().Exists(c => c.AppName.Equals(ddlappname.SelectedItem.Text, StringComparison.OrdinalIgnoreCase)))
                {
                    RegisteredApplications selectedApplication = AppDB.securityDB.RegApps.ToList().Where(c => c.AppName.Equals(ddlappname.SelectedItem.Text, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

                    if (selectedApplication.ConfidentialKey == null)
                    {
                        AppDB.Logger.Info("Checking for Confidential key exists in DB...");
                        if (!AppDB.securityDB.ConfidentialKeys.ToList().Exists(c => c.SecretKey.Equals(txtsecretkey.Text)))
                        {
                            AppDB.Logger.Info("New Confidential key detected. Initiating Saving...");
                            lock (lockTarget)
                            {
                                ConfidentialKey confKey = new ConfidentialKey();

                                bool findFreshKey = true;
                                while (findFreshKey)
                                {
                                    long key = new Random().Next(1000, 99999);
                                    if (!AppDB.securityDB.ConfidentialKeys.ToList().Exists(c => c.KeyID.Equals(key)))
                                    {
                                        findFreshKey = false;
                                        confKey.KeyID = key;
                                    }
                                }
                                confKey.SecretKey = txtsecretkey.Text;
                                selectedApplication.ConfidentialKey = confKey;
                                AppDB.securityDB.SaveChanges();
                                myfashionsServerSDK.RefreshAPI.RefreshWEBAPI refreshapi = new myfashionsServerSDK.RefreshAPI.RefreshWEBAPI(ip, port);
                                refreshapi.RefreshDB();
                            }
                            AppDB.Logger.Info("Saved.");
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Confidential Key Created Successfully'); window.location='" + Request.ApplicationPath + "ViewConfidentialKeys.aspx';", true);
                        }
                    }
                    else
                    {
                        AppDB.Logger.Info("Already Confidential key exist.");
                        ddlappname.ClearSelection();
                        ddlappname.Items.FindByText("Select").Selected = true;
                        txtsecretkey.Text = "";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Already confidential key exist for this application .');", true);
                    }
                }
                else
                {
                    AppDB.Logger.Info("Invalid Registered Application.");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Invalid Registered Application.');", true);
                }

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Required Fileds should not be Empty.');", true);
            }

        }
        catch (Exception ex)
        {
            AppDB.Logger.ErrorException(ex.Message, ex);
        }
      
    }

    private void clearAll()
    {
        txtsecretkey.Text = "";
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("ViewConfidentialKeys.aspx");
    }
}